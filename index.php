<?php
/**
 * The backbone and foundation of this application, without this the world would
 * end and everything we tried to archieve would be lost.
 *
 * not this dramatic but this is a pretty important file so edit with
 * caution
 */
session_start();
require_once("./src/autoload.php");

// Easy to use functions because we are lazy and
// dont want to write the whole path to the class everytime
require_once("./src/functions.php");

require_once("routes.php");

// Initialize dabatase
$generator = new \Database\DatabaseGenerator();
$generator->generateDatabase();

set_time_limit(60*60);
// Initialize the application
$application = new Application;
$application->run();
set_time_limit(30);
