<?php
/**
 * Routes for this applications
 * to add a new route use the key as route name
 */

route("home", new \Routing\Route("/", "\Controllers\Home", "start", "GET"));

/* User routes */
route("user_create", new \Routing\Route("/user/register", "\Controllers\User", "create", "GET"));
route("user_profile", new \Routing\Route("/user/:id", "\Controllers\User", "show", "GET", [":id" => "([0-9]+)"]));
route("user_update", new \Routing\Route("/user/:id/edit", "\Controllers\User", "edit", "GET", [":id" => "([0-9]+)"]));
route("user_update_post", new \Routing\Route("/user/edit", "\Controllers\User", "update", "POST"));
route("user-login", new \Routing\Route("/user/login", "\Controllers\User", "signin", "GET"));
route("user-login-action", new \Routing\Route("/user/login", "\Controllers\User", "login", "POST"));
route("user_logout", new \Routing\Route("/user/logout", "\Controllers\User", "logout", "GET"));
route("user_create_post", new \Routing\Route("/user/register", "\Controllers\User", "add", "POST"));

/* Book routes */
route("books", new \Routing\Route("/books", "\Controllers\Book", "all", "GET"));
route("book", new \Routing\Route("/book/:id", "\Controllers\Book", "show", "GET", [":id" => "([0-9]+)"]));
route("book_like", new \Routing\Route("/like/add", "\Controllers\Book", "like", "POST"));
route("book_like_status", new \Routing\Route("/like/status", "\Controllers\Book", "likeStatus", "POST"));
route("book_read", new \Routing\Route("/read/add", "\Controllers\Book", "read", "POST"));
route("book_read_status", new \Routing\Route("/read/status", "\Controllers\Book", "readStatus", "POST"));
route("book_readlist", new \Routing\Route("/readlist/add", "\Controllers\Book", "readlist", "POST"));
route("book_readlist_status", new \Routing\Route("/readlist/status", "\Controllers\Book", "readlistStatus", "POST"));


/* Author Routes */
route("authors", new \Routing\Route("/authors", "\Controllers\Author", "all", "GET"));
route("author", new \Routing\Route("/author/:id", "\Controllers\Author", "show", "GET", [":id" => "([0-9]+)"]));

/* Genre routes */
route("genres", new \Routing\Route("/genres", "\Controllers\Genre", "all", "GET"));
route("genre", new \Routing\Route("/genre/:id", "\Controllers\Genre", "show", "GET", [":id" => "([0-9]+)"]));

/* Serie routes */
route("series", new \Routing\Route("/series", "\Controllers\Serie", "all", "GET"));
route("serie", new \Routing\Route("/serie/:id", "\Controllers\Serie", "show", "GET", [":id" => "([0-9]+)"]));

/* Search routes */
route("suggestions", new \Routing\Route("/search/suggestions", "\Controllers\Search", "suggestions", "GET"));
route("search", new \Routing\Route("/search", "\Controllers\Search", "search", "GET"));

/* Friend links */
route("friends", new \Routing\Route("/user/:id/friends","\Controllers\Friends","all", "GET" ,[":id" => "([0-9]+)"]));
route("friend_requests", new \Routing\Route("/friends/request", "\Controllers\Friends","requests","GET"));
route("friend_request_accept", new \Routing\Route("/friends/request/accept", "\Controllers\Friends","accept","POST"));
route("friend_request_decline", new \Routing\Route("/friends/request/decline", "\Controllers\Friends","decline","POST"));

route("friend_request_send", new \Routing\Route("/friends/request/send", "\Controllers\Friends","send","POST"));
route("friend_suggestions", new \Routing\Route("/friends/suggestions", "\Controllers\Friends","suggestions","GET"));


/* reviews */
route("review_create_post", new \Routing\Route("/review/add", "\Controllers\Review", "add", "POST"));
route("review_update", new \Routing\Route("/review/:id/edit", "\Controllers\Review", "edit", "GET", [":id" => "([0-9]+)"]));
route("review_update_post", new \Routing\Route("/review/update", "\Controllers\Review", "update", "POST"));
route("review_delete", new \Routing\Route("/review/:id/delete", "\Controllers\Review", "remove", "GET", [":id" => "([0-9]+)"]));


// \Routes::post("review", new \Routing\Route("/review/:id", "\Controllers\Review", "show", "GET", [":id" => "([0-9]+)"]));


/* Recommender routes */
route("user_based_book", new \Routing\Route("/recommender/user", "\Controllers\Home", "userBasedBook", "GET"));
route("item_based_book", new \Routing\Route("/recommender/items", "\Controllers\Home", "itemBasedBook", "GET"));
route("stats", new \Routing\Route("/stats/:id", "\Controllers\Home", "statistics", "GET", [":id" => "([0-9]+)"]));
route("stats_csv", new \Routing\Route("/stats/:id/csv", "\Controllers\Home", "CSVStatistics", "GET", [":id" => "([0-9]+)"]));
