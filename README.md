Who made this application?

* Robbert Stevens - s3041638
* Jorrit Bakker - s1857843

How to start the application the first time?

1. create an empty database in MySQL
2. open config.ini (if it does not exist yet you can create it in the root directory)
3. edit the fields necessary for your local environment(host,dbname, username, password)
4. go to www.yourdomain.com/ (or wherever you want to try the application)
5. wait a moment, we are setting up the application(can take a while because we are inserting a lot of stuff in the database)
6. once loaded enjoy this beautiful application


Scripts we just got from the internet are:

* Faker
* Autoloader
