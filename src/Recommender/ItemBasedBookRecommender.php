<?php namespace Recommender;

/**
*
* @author Jorrit Bakker
*/


class ItemBasedBookRecommender extends \Recommender\IRecommender
{

    public function transformPreferences()
    {

    }

    /**
    * calculates the cosine
    * @param $x User one
    * @param $y User two
    * @return simalarity
    */
    public function computeSimalarity($usersForBook, $nlBook)
    {
      $teller = 0;
      $noemer1 = 0;
      $noemer2 = 0;

      $usersForBook = array_values($usersForBook);
      $nlBook = array_values($nlBook);

      for($i = 0; $i < count($usersForBook); $i++){
        $teller += $usersForBook[$i]*$nlBook[$i];
        $noemer1 += $usersForBook[$i]* $usersForBook[$i];
        $noemer2 += $nlBook[$i]* $nlBook[$i];
      }


      $noemer = sqrt($noemer1) * sqrt($noemer2);
      if($noemer == 0){
        return 0;
      }

      $cosine = $teller / (sqrt($noemer1) * sqrt($noemer2));

      return $cosine;

    }

    /**
    * calculates the prediction between 2 users
    */
    public function computePrediction($simalarity, $centeredMatrix, $matrix, $userId)
    {
      $kNearestMeans = 20;

      arsort($simalarity);

      $teller = 0;
      $noemer = 0;

      $i = 0;
      foreach($simalarity as $bookId => $cosine){
        if($i > $kNearestMeans) break;

        $teller += $matrix[$userId][$bookId]*$cosine;
        $noemer += $cosine;

        $i++;
      }

      if($noemer == 0){
        return 0;
      }
      $prediction = $teller/$noemer;

      return $prediction;

    }

    /**
    * rewrites the matrix
    * @param $matrix
    * @return $matrix
    */
    public function rewriteMatrix($centeredMatrix)
    {
      $newMatrix = [];

      foreach($centeredMatrix as $user => $books){ // $array[$user][bookid] = centered value
          foreach($books as $bookId => $centered){
            $newMatrix[$bookId][$user] = $centered;
          }
      }
      return $newMatrix;
    }

    public function matchItems()
    {

    }

    /**
    * @param $user User ID
    */

    public function getRecommendations($userId)
    {
      $books = \Database\RepositoryFactory::book();
      $users = \Database\RepositoryFactory::user();

      $matrix = $this->getMatrix($books, $users);

      $recommendation = [];

      $centeredMatrix = $this->centering($matrix, $userId); //$array[userId] = [bookId => float];
      $newMatrix = $this->rewriteMatrix($centeredMatrix);

      foreach($books->all() as $nlBook)
      {

        foreach($newMatrix as $book => $users){
          if($book != $nlBook->id){

            $simalarity[$book] = $this->computeSimalarity($users, $newMatrix[$nlBook->id]);

          }
        }

        $prediction = $this->computePrediction($simalarity, $newMatrix, $matrix, $userId);
        // if($prediction > 0){
          $recommendation[$nlBook->id] = [$prediction, $nlBook];
        // }
      }

      uasort($recommendation, function($a, $b) {
          return $b[0] > $a[0] ? 1 : -1;
      });

      return $recommendation;


    }

}
