<?php namespace Recommender;

/**
*
* @author Jorrit Bakker | Robbert Stevens
*/
abstract class IRecommender
{
  /**
  * Create the matrix
  * @return Array
  */
  public function getMatrix($books, $users)
  {

      // Haal alle users op
      // Haal alle likes op van like tabel
      // Ga Pearson berekenen van alles users tenopzichte je $id
      // =(3/4)+(0,57*0,1+1*0/(1+0,57))

      $allBooks = $books->all();
      $getLikes = $books->findAllLikes();
      $likedBooks = [];

      foreach ($getLikes as $key => $value) {
          $likedBooks[$value->userId][] = $value->bookId;
      }
      foreach($users->all() as $user)
      {
          $liked[$user->id] = [];
          foreach ($allBooks as $book)
          {
              $b = 0;
              if (isset($likedBooks[$user->id]))
              {
                  if(in_array($book->id, $likedBooks[$user->id]))
                  {
                      $b = 1;
                  }
              }
              $liked[$user->id][$book->id] = $b;
          }
      }
      return $liked;
  }

  /**
  * Center the matrix
  * @param $matrix Array
  * @return Array
  */
  public function centering($matrix)
  {
      $centeredMatrix = [];
      foreach ($matrix as $userId => $books)
      {
          //var_dump($books);
          $sum = array_sum($books);
          $centered = $sum / count($books);
          foreach ($books as $bookId => $like)
          {
              $centeredMatrix[$userId][$bookId] = $like - $centered;
          }

      }
      return $centeredMatrix;
  }
  
  /**
   * Get recommendations
   * @return Array Model/Book
   */
  public abstract function getRecommendations($userId);
}
