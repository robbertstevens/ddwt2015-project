<?php namespace Recommender;
/**
* User based recommender
* @author Robbert Stevens
*/
class UserBasedBookRecommender extends \Recommender\IRecommender
{

    /**
    * calculates the pearson correlation between 2 users
    * @param $x User one
    * @param $y User two
    * @return ???
    */
    private function computeSimalarity($x, $y)
    {
        $sum_x = $sum_y = $sqr_x = $sqr_y = $sum_xy = 0;
        for ($i=1; $i <= count($x); $i++) {
            $sum_x += $x[$i];
            $sum_y += $y[$i];
            $sum_xy += $x[$i] * $y[$i];
            $sqr_x += $x[$i] ** 2;
            $sqr_y += $y[$i] ** 2;
        }
        $n = count($x);

        $teller = $n * $sum_xy - ($sum_x * $sum_y);
        $noemer = sqrt(($n * $sqr_x - $sum_x ** 2) * ($n * $sqr_y - $sum_y ** 2));

        if ($noemer == 0) return 0;

        return $teller / $noemer;
    }
    /**
    * som($pearson * centerscore)  / som($pearson) + avgcenteredScore(currentBOok)
    * @param $simalarity Array Key userId, value Pearson
    */
    public function computePrediction($simalarity, $centeredMatrix, $matrix, $userId, $bookId)
    {
        arsort($simalarity);
        $count = 0;
        $meanScore = 0;
        foreach ($matrix[$userId] as $book) {
            $meanScore += $book;
        }
        $meanScore = $meanScore / count($matrix[$userId]);

        $teller = 0;
        $noemer = 0;

        foreach ($simalarity as $user => $pearson) {
            if($count > 20) break;
            $teller += $pearson * $matrix[$user][$bookId];
            $noemer += $pearson;

            $count ++;
        }
        if ( $noemer == 0) return 0;

        $prediction = $meanScore + ($teller / $noemer);

        return $prediction;
    }

    /**
    * @param $user User ID
    * @param $book Book ID
    */
    public function getRecommendations($userId)
    {

        //return $this->computeSimalarity($id);
        $books = \Database\RepositoryFactory::book();
        $users = \Database\RepositoryFactory::user();

        $matrix =  $this->getMatrix($books, $users);
        $recommendation = [];


        $specificMatrix = $matrix;
        foreach ($specificMatrix as $user => $array)
        {
            if ( $user != $userId) {
                $simalarity[$user] = $this->computeSimalarity($array, $specificMatrix[$userId]);
            }
        }

        foreach($books->findNotLikedByUser($userId) as $book)
        //foreach($books->all() as $book)
        {
            $prediciton = $this->computePrediction($simalarity, $specificMatrix, $matrix, $userId, $book->id);

            //if ( $prediciton > .4 )
                $recommendation[$book->id] = [$prediciton, $book];

        }
        uasort($recommendation, function($a, $b) {
            return $b[0] > $a[0] ? 1 : -1;
        });

        return $recommendation;

    }
}
