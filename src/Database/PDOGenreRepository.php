<?php namespace Database;

/**
* @author Jorrit Bakker
* @version 0.1
**/

class PDOGenreRepository implements \Database\Interfaces\IGenreRepository
{
  private $model = "\Models\Genre";

  /**
  * Return a genre from the database
  * @param $id Genre id
  * @return Models/Genre
  */
  public function find($id)
  {
    $query = "SELECT * FROM genre WHERE id = :id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }

  /**
  * Return all genres from the database
  * @return Models/Genre
  */
  public function all($limit = null, $offset = null)
  {
    $query = "SELECT * FROM genre ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();
    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  /**
  * Return a genre from the database
  * @param $id Genre id
  * @return Models/Genre
  */
  public function remove($id)
  {
    $query = "DELETE FROM genre WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $id);
    return $stm->execute();
  }

  /**
  * Create a genre
  * @param $model Genre object
  * @return Models/Genre
  */
  public function create($model)
  {
    $query = "INSERT INTO genre (name) VALUES (:name)";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":name", $model->name);
    return $stm->execute();
  }

  /**
  * Return a genre from the database
  * @param $id Genre id
  * @return Models/Genre
  */
  public function update($model)
  {
    $query = "UPDATE genre SET name=:name WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $model->id);
    $stm->bindParam(":name", $model->name);
    return $stm->execute();
  }

  /**
   * Find genre by name
   * @param $name string
   * @return Model/Genre
   */
  public function findByName($name){
    $query = "SELECT FROM genre WHERE name=:name";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":name", $model->name);
    $stm->execute();
    return $stm->fetchObject($this->model);
  }

  /**
   * Find all genres with names like @param
   * @param $like string
   * @return Array Model/Genre
   */
  public function findAllByNameLike($like, $limit = null, $offset = null)
  {
    $query = "SELECT * FROM genre WHERE name LIKE :name ";

    $parameter = '%'.$like.'%';
    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    $stm->bindParam(":name", $parameter);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  /**
   * Find all genres by book Id
   * @param $id Book Id
   * @return Array Model/Genre
   */
  public function findGenreByBook($id, $limit = null, $offset = null)
  {
    $query = "SELECT genre.id, name FROM genre JOIN book_genre ON (genre.id = book_genre.genreId) WHERE book_genre.bookId = :id ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    $stm->bindParam(":id", $id);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }
}
