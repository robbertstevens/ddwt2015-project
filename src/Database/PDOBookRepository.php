<?php namespace Database;

/**
* @author Robbert Stevens
* @version 0.1
**/
class PDOBookRepository implements \Database\Interfaces\IBookRepository
{
  private $model = "\Models\Book";

  /**
  * Return a book with a certain ID
  * @param $id ID of the book to find
  * @return Models/Book
  */
  public function find($id)
  {
    $query = "SELECT * FROM book WHERE id = :id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }

  /**
  * Returns all books in the database
  * @return Array Models/Book
  */
  public function all($limit = null, $offset = null)
  {
    $query = "SELECT * FROM book ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();
    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  /**
  * Inserts the given model and returns the ID of the inserted row
  * @return int ID of the row edited
  */
  public function create($model)
  {
    $query = "INSERT INTO book (name, pages, image, description) VALUES (:name, :pages, :image, :description)";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":name", $model->name);
    $stm->bindParam(":pages", $model->pages);
    $stm->bindParam(":image", $model->image);
    $stm->bindParam(":description", $model->description);
    $stm->execute();

    $lastId = pdo()->lastInsertId();
    return $lastId;
  }

  /**
  * @param $id Id of the book to be removed
  * @return boolean
  */
  public function remove($id)
  {
    $query = "DELETE FROM book WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $model->id);

    return $stm->execute();
  }
  /**
  * Updates book model
  * @param $model Id of the book to be update
  * @return boolean
  */
  public function update($model)
  {
    $query = "UPDATE book SET name=:name, pages=:pages, image=:image, description=:description WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $model->id);
    $stm->bindParam(":name", $model->name);
    $stm->bindParam(":pages", $model->pages);
    $stm->bindParam(":image", $model->image);
    $stm->bindParam(":description", $model->description);

    return $stm->execute();
  }
  /**
   * Find all books with name like @param
   * @param $like string
   * @return Array Model/Book
   */
  public function findAllByNameLike($like, $limit = null, $offset = null)
  {
    $query = "SELECT * FROM book WHERE name LIKE :like ";
    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);


    $like = "%$like%";
    $stm->bindParam(":like", $like);
    $stm->execute();


    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }
  /**
   * Find book by name
   * @param $name string
   * @return Model/Book
   */
  public function findByName($name, $limit = null, $offset = null)
  {
    $query = "SELECT * FROM book WHERE name=:name";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->bindParam(":name", $name);
    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);

  }

  /**
   * Find books that a user likes
   * @param $id User Id
   * @return Array Model/Book
   */
  public function findLikesByUser($id, $limit = null, $offset = null)
  {
    $query = "SELECT `book`.id, name, pages, image, description FROM `book` JOIN `like` ON (`book`.id=`like`.bookId) WHERE `like`.userId = :id";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  /**
   * Find books that a user not liked
   * @param $id User Id
   * @return Array Model/Book
   */
  public function findNotLikedByUser($id, $limit = null, $offset = null)
  {
    $query = "SELECT `book`.id, name, pages, image, description FROM `book` WHERE `book`.id NOT IN (SELECT bookId FROM `like` WHERE `like`.userId = :id)";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }
  /**
   * Find books written by an author
   * @param $id Author Id
   * @return Array Model/Book
   */
  public function findBooksByAuthor($id, $limit = null, $offset = null)
  {
    $query = "SELECT book.id, name, pages, image, description FROM `book` JOIN `book_author` ON (`book`.id = `book_author`.bookId) WHERE `book_author`.authorId = :id ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  /**
   * Find books in a certain genre
   * @param $id Genre Id
   * @return Array Model/Book
   */
  public function findBooksByGenre($id, $limit = null, $offset = null)
  {
    $query = "SELECT book.id, name, pages, image, description FROM `book` JOIN book_genre ON (`book`.id = `book_genre`.bookId) WHERE `book_genre`.genreId = :id ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  /**
   * Find books in a certain serie
   * @param $id Serie Id
   * @return Array Model/Book
   */
  public function findBooksBySerie($id, $limit = null, $offset = null)
  {
    $query = "SELECT book.id, name, pages, image, description FROM `book` JOIN book_serie ON (`book`.id = `book_serie`.bookId) WHERE `book_serie`.serieId = :id ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  /**
   * Find books on readlist of User
   * @param $id User Id
   * @return Array Model/Book
   */
   public function findBooksOnReadlistUser($id, $limit = null, $offset = null)
   {
     $query = "SELECT book.id, name, pages, image, description FROM `book` JOIN `readlist` ON (`book`.id = `readlist`.bookId) WHERE `readlist`.userId = :id ";

     if(!is_null($limit))
       $query .= "LIMIT :limit ";

     if(!is_null($offset))
       $query .= "OFFSET :offset";

     $stm = pdo()->prepare($query);

     if(!is_null($limit))
       $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

     if(!is_null($offset))
       $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

     $stm->bindParam(":id", $id);
     $stm->execute();

     return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
   }

   /**
    * Finds book read by user
    * @param $id User id
    * @return Array Models/Book
    */
   public function findBooksReadByUser($id, $limit = null, $offset = null)
   {
     $query = "SELECT book.id, name, pages, image, description FROM `book` JOIN `read` ON (`book`.id = `read`.bookId) WHERE `read`.userId = :id ";

     if(!is_null($limit))
       $query .= "LIMIT :limit ";

     if(!is_null($offset))
       $query .= "OFFSET :offset";

     $stm = pdo()->prepare($query);

     if(!is_null($limit))
       $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

     if(!is_null($offset))
       $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

     $stm->bindParam(":id", $id);
     $stm->execute();

     return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
   }

   public function findAllLikes($limit = null, $offset = null)
   {
     $query = "SELECT userId, bookId FROM `like`";

     if(!is_null($limit))
       $query .= "LIMIT :limit ";

     if(!is_null($offset))
       $query .= "OFFSET :offset";

     $stm = pdo()->prepare($query);

     if(!is_null($limit))
       $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

     if(!is_null($offset))
       $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

     $stm->bindParam(":id", $id);
     $stm->execute();

     return $stm->fetchAll(\PDO::FETCH_CLASS);
   }
   /**
    * Finds book read by user
    * @param $id User id
    * @return Array Models/Book
    */
   public function findAllLikesByUser($id, $limit = null, $offset = null)
   {
     $query = "SELECT book.id, name, pages, image, description FROM `book` JOIN `like` ON (`book`.id = `like`.bookId) WHERE `like`.userId =:id ";

     if(!is_null($limit))
       $query .= "LIMIT :limit ";

     if(!is_null($offset))
       $query .= "OFFSET :offset";

     $stm = pdo()->prepare($query);

     if(!is_null($limit))
       $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

     if(!is_null($offset))
       $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

     $stm->bindParam(":id", $id);
     $stm->execute();

     return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
   }
   public function findLikeByUser($user, $book)
   {
     $query = "SELECT * FROM `like` WHERE bookId = :book AND userId = :user";


     $stm = pdo()->prepare($query);
     $stm->bindParam(":user", $user);
     $stm->bindParam(":book", $book);
     $stm->execute();

     return $stm->fetch() > 0;

   }
   /**
    * adds like for book by user
    * @param $user User id
    * @param $book Book id
    * @return like id
    */
   public function addLikeFor($user, $book)
   {
     $query = "INSERT INTO `like` (`userId`, `bookId`, `date`) VALUES (:userId, :bookId, NOW())";
     $stm = pdo()->prepare($query);
     $stm->bindParam(":userId", $user);
     $stm->bindParam(":bookId", $book);

     return $stm->execute();
   }

   /**
    * removes like for book by user
    * @param $id like id
  */
   public function removeLikeFor($user, $book)
   {
     $query = "DELETE FROM `like` WHERE userId = :userId AND bookId = :bookId";
     $stm = pdo()->prepare($query);
     $stm->bindParam(":userId", $user);
     $stm->bindParam(":bookId", $book);
    return  $stm->execute();
   }

   /**
    * a
    * @param $author book_author id
  */
   public function addAuthorFor($authorId, $bookId)
   {
     $query = "INSERT INTO `book_author` (`bookId`, `authorId`) VALUES (:bookId, :authorId)";
     $stm = pdo()->prepare($query);
     $stm->bindParam(":authorId", $authorId);
     $stm->bindParam(":bookId", $bookId);

     return $stm->execute();
   }

   /**
    * removes like for book by user
    * @param $id book_author id
  */
   public function removeAuthorFor($authorId, $bookId)
   {
     $query = "DELETE FROM `book_author` WHERE authorId=:authorId AND bookId=:bookId";
     $stm = pdo()->prepare($query);
     $stm->bindParam(":authorId", $authorId);
     $stm->bindParam(":bookId", $bookId);
     return $stm->execute();
   }


   /**
    * adds genre for book by id
    * @param $genreId id, $bookId id
  */
   public function addGenreFor($genreId, $bookId)
   {
     $query = "INSERT INTO `book_genre` (`bookId`, `genreId`) VALUES (:bookId, :genreId)";
     $stm = pdo()->prepare($query);
     $stm->bindParam(":genreId", $genreId);
     $stm->bindParam(":bookId", $bookId);

     return $stm->execute();
   }

   /**
    * removes genre for book by id
    * @param $genreId id, $bookId id
  */
   public function removeGenreFor($genreId, $bookId)
   {
     $query = "DELETE FROM `book_genre` WHERE genreId=:genreId AND bookId=:bookId";
     $stm = pdo()->prepare($query);
     $stm->bindParam(":genreId", $genreId);
     $stm->bindParam(":bookId", $bookId);

     return $stm->execute();
   }

   /**
    * Adds a book to a user's readlist
    * @param $book the book Id
    * @param $user  the user Id
    */
   public function addToUserReadlist($user, $book) {
     $query = "INSERT INTO `readlist` (`bookId`, `userId`) VALUES (:bookId, :userId)";
     $stm = pdo()->prepare($query);

     $stm->bindParam(":userId", $user);
     $stm->bindParam(":bookId", $book);

     return $stm->execute();
   }

   /**
    * Remove from user readlist
    * @param $book the book Id
    * @param $user  the user Id
    */
   public function removeFromUserReadlist($user, $book) {
     $query = "DELETE FROM `readlist` WHERE userId=:userId AND bookId=:bookId";
     $stm = pdo()->prepare($query);

     $stm->bindParam(":userId", $user);
     $stm->bindParam(":bookId", $book);
     return $stm->execute();

   }

     /**
      * Find book from user readlist
      * @param $book the book Id
      * @param $user  the user Id
      */
    public function findReadByUser($user, $book){

      $query = "SELECT * FROM `read` WHERE bookId = :book AND userId = :user";

      $stm = pdo()->prepare($query);
      $stm->bindParam(":user", $user);
      $stm->bindParam(":book", $book);
      $stm->execute();

      return $stm->fetch() > 0;
    }

    /**
     * Find book from user readlist
     * @param $book the book Id
     * @param $user  the user Id
     */

    public function findBookOnReadlistByUser($user, $book){

      $query = "SELECT * FROM `readlist` WHERE bookId = :book AND userId = :user";

      $stm = pdo()->prepare($query);
      $stm->bindParam(":user", $user);
      $stm->bindParam(":book", $book);
      $stm->execute();

      return $stm->fetch() > 0;

    }


    public function addReadFor($userId, $bookId)
    {
      $query = "INSERT INTO `read` (`bookId`, `userId`) VALUES (:bookId, :userId)";
      $stm = pdo()->prepare($query);
      $stm->bindParam(":userId", $userId);
      $stm->bindParam(":bookId", $bookId);
      return $stm->execute();
    }

    /**
     * removes genre for book by id
     * @param $genreId id, $bookId id
      */
    public function removeReadFor($userId, $bookId)
    {
      $query = "DELETE FROM `read` WHERE userId=:userId AND bookId=:bookId";
      $stm = pdo()->prepare($query);
      $stm->bindParam(":userId", $userId);
      $stm->bindParam(":bookId", $bookId);
      return $stm->execute();
    }
    /**
     * Get total amount of likes of all books together
     * @return integer Total amount of likes
     */
    public function getTotalLikes()
    {
      $query = "SELECT count(*) FROM `like`";
      $stm = pdo()->prepare($query);
      $stm->execute();

      return $stm->fetchColumn();
    }

    /**
     * Get total amount of books read for all users
     * @return integer Total amounts of read books
     */
    public function getTotalBooksRead()
    {
      $query = "SELECT count(*) FROM `read`";
      $stm = pdo()->prepare($query);
      $stm->execute();

      return $stm->fetchColumn();
    }
}
