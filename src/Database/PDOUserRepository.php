<?php namespace Database;

class PDOUserRepository implements \Database\Interfaces\IUserRepository
{
  private $model = "\Models\User";

  public function create($model)
  {
    $query = "INSERT INTO user (email, name, password) VALUES (:email, :name, :password)";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":email", $model->email);
    $stm->bindParam(":name", $model->name);
    $stm->bindParam(":password", $model->getPassword());
    return $stm->execute();
  }

  public function find($id)
  {
    $query = "SELECT * FROM user WHERE id = :id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }
  public function all($limit = null, $offset = null)
  {
    $query = "SELECT * FROM user ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();
    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }
  public function remove($id)
  {
    $query = "DELETE FROM user WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $model->id);
    return $stm->execute();
  }

  public function update($model)
  {
    $query = "UPDATE user SET name=:name, password=:password, dob=:dob, gender=:gender, oneliner=:oneliner  WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $model->id);
    $stm->bindParam(":name", $model->name);
    $stm->bindParam(":password", $model->getPassword());
    $stm->bindParam(":dob", $model->dob);
    $stm->bindParam(":gender", $model->gender);
    $stm->bindParam(":oneliner", $model->oneliner);
    return $stm->execute();

  }

  public function findAllByNameLike($like, $limit = null, $offset = null)
  {
    $query = "SELECT * FROM user WHERE name LIKE :name ";

    $parameter = '%'.$like.'%';
    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    $stm->bindParam(":name", $parameter);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }


  public function findByName($name)
  {
    $query = "SELECT * FROM user WHERE name LIKE :name ";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":name", $name);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }
  public function findByEmail($email)
  {
    $query = "SELECT * FROM user WHERE email LIKE :email ";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":email", $email);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }

  public function findUserFriends($id, $limit = null, $offset = null)
  {
    $query = "SELECT user.id, name, email, password, gender, dob, oneliner FROM user WHERE user.id IN (
                SELECT friendId AS fid FROM
                	(
                    (SELECT DISTINCT friendId FROM friend WHERE userId = :id)
                    UNION ALL
                    (SELECT DISTINCT userId from friend WHERE friendId = :id)
                  ) AS tbl GROUP BY fid HAVING COUNT(*) = 2
                )";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    $stm->bindParam(":id", $id);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  public function findFriendRequestFor($id, $limit = null, $offset = null)
  {
    $query = "SELECT user.id, name, email, password, gender, dob, oneliner
              FROM user
              WHERE user.id NOT IN (
                SELECT friendId AS fid
                FROM (
                  (
                    SELECT DISTINCT friendId
                    FROM friend
                    WHERE userId = :id
                  )
                  UNION ALL (
                    SELECT DISTINCT userId
                    from friend
                    WHERE friendId = :id
                  )
                ) AS tbl GROUP BY fid HAVING COUNT(*) = 2 )
                AND user.id IN (
                  SELECT userId
                  FROM friend
                  WHERE friendId = :id
                )";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    $stm->bindParam(":id", $id);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  public function findLikesForBook($id, $limit = null, $offset = null)
  {
    $query = "SELECT user.id, email, password, name FROM user JOIN `like` ON (user.id = `like`.userId) WHERE bookId = :id ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    $stm->bindParam(":id", $id);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();

    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
  }

  public function findUserByEmail($email)
  {
    $query = "SELECT * FROM user WHERE email = :email";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":email", $email);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }

  public function addFriendFor($userId, $friendId){
    $query = "INSERT INTO friend (userId, friendId) VALUES (:userId , :friendId)";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":userId", $userId);
    $stm->bindParam(":friendId", $friendId);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }
  public function declineFriendFor($user, $friend)
  {
    $query = "DELETE FROM friend WHERE userId = :userId and friendId = :friendId";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":userId", $friend);
    $stm->bindParam(":friendId", $user);
    return $stm->execute();
  }
}
