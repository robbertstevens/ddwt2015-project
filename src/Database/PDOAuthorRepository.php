<?php namespace Database;

/**
 * Implementation of the IAuthorRepository Interfaces
 * @author Robbert Stevens
 */

 class PDOAuthorRepository implements \Database\Interfaces\IAuthorRepository
 {
     private $model = "\Models\Author";
     public function create($model)
     {
       $query = "INSERT INTO author (name, description) VALUES (:name, :description)";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":name", $model->name);
       $stm->bindParam(":description", $model->description);
       $stm->execute();

       return pdo()->lastInsertId();
     }

     public function find($id)
     {
       $query = "SELECT * FROM author WHERE id = :id";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":id", $id);
       $stm->execute();

       return $stm->fetchObject($this->model);
     }

     public function all($limit = null, $offset = null)
     {
       $query = "SELECT * FROM author ";

       if(!is_null($limit))
         $query .= "LIMIT :limit ";

       if(!is_null($offset))
         $query .= "OFFSET :offset";

       $stm = pdo()->prepare($query);

       if(!is_null($limit))
         $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

       if(!is_null($offset))
         $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

       $stm->execute();
       return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
     }

     public function remove($id)
     {
       $query = "DELETE FROM author WHERE id = :id";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":id", $id);
       return $stm->execute();
     }

     public function update($model)
     {
       $query = "UPDATE author SET name=:name, surname=:description WHERE id=:id";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":id", $model->id);
       $stm->bindParam(":name", $model->name);
       $stm->bindParam(":description", $model->description);
       return $stm->execute();
     }

     public function findAuthorsByBook($id, $limit = null, $offset = null)
     {
          $query = "SELECT author.id, name, description FROM author JOIN book_author ON (author.id = book_author.authorId) WHERE book_author.bookId = :id ";

          if(!is_null($limit))
               $query .= "LIMIT :limit ";

          if(!is_null($offset))
               $query .= "OFFSET :offset";


          $stmt = pdo()->prepare($query);
          $stmt->bindParam(":id", $id);

          if(!is_null($limit))
               $stmt->bindParam(":limit", $limit);

          if(!is_null($offset))
               $stmt->bindParam(":offset", $offset);

          $stmt->execute();

          return $stmt->fetchAll(\PDO::FETCH_CLASS, $this->model);
     }

     public function findAllByNameLike($like, $limit = null, $offset = null)
     {
          $query = "SELECT * FROM author WHERE name LIKE :like ";
          $parameter = "%" . $like ."%";
          if(!is_null($limit))
            $query .= "LIMIT :limit ";

          if(!is_null($offset))
            $query .= "OFFSET :offset";

          $stm = pdo()->prepare($query);
          $stm->bindParam(":like", $parameter);
          if(!is_null($limit))
            $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

          if(!is_null($offset))
            $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

          $stm->execute();
          return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
     }
 }
