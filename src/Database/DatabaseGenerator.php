<?php namespace Database;

/**
* @author Jorrit Bakker
* @version 0.1
**/

class DatabaseGenerator{
  private $faker;

  // settings
  private $num_of_users;
  private $num_of_books;
  private $num_of_authors;
  private $num_of_series;

  private $likes_per_user;
  private $friends_per_user;
  private $genres_per_book;
  private $series_per_book;
  private $authors_per_book;

  public function __construct()
  {
    $config = \Util\Config::getInstance();
    $this->num_of_users = $config->settings["gen"]["num_of_users"];
    $this->num_of_books = $config->settings["gen"]["num_of_books"];
    $this->num_of_authors = $config->settings["gen"]["num_of_authors"];
    $this->num_of_series = $config->settings["gen"]["num_of_authors"];
    
    $this->likes_per_user = array($config->settings["gen"]["likes_per_user_min"], $config->settings["gen"]["likes_per_user_max"]);
    $this->friends_per_user = array($config->settings["gen"]["friends_per_user_min"], $config->settings["gen"]["friends_per_user_max"]);
    $this->genres_per_book = array($config->settings["gen"]["genres_per_book_min"], $config->settings["gen"]["genres_per_book_max"]);
    $this->series_per_book = array($config->settings["gen"]["series_per_book_min"], $config->settings["gen"]["series_per_book_max"]);
    $this->authors_per_book = array($config->settings["gen"]["authors_per_book_min"], $config->settings["gen"]["authors_per_book_max"]);

    $this->faker = \Faker\Factory::create();
    $this->faker->seed(1234);
  }

  public function generateDatabase()
  {
    set_time_limit(60 * 60);
    $this->createTables();
    $this->createUsers();
    $this->createAuthors();
    $this->createGenres();
    $this->createSeries();
    $this->createBooks();
    set_time_limit(30);
  }

  private function createTables()
  {
    $sql = file_get_contents('./resources/mysql.sql');
    $stmt = pdo()->prepare($sql);
    $stmt->execute();
  }

  private function createUsers()
  {

    $userRepo = new PDOUserRepository();

    if(count($userRepo->all()) == 0)
    {
      for($i = 0; $i < $this->num_of_users; $i++)
      {
        $user = new \Models\User;
        $user->name = $this->faker->name;
        $user->email = $this->faker->email;
        $user->password("password");

        $userRepo->create($user);

      }

      $this->addRandomFriends();

    }

  }

  private function createAuthors()
  {

    $authorRepo = new PDOAuthorRepository();

    if(count($authorRepo->all()) == 0)
    {
      for($i = 0; $i < $this->num_of_authors; $i++)
      {

        $author = new \Models\Author;
        $author->name = $this->faker->name;
        $author->description = $this->faker->text(300);

        $authorRepo->create($author);
      }
    }

  }

  private function createGenres()
  {
    $genres = ["Thriller", "Roman", "Sci-fi", "Detective", "Fantasy", "Horror", "Biografie"];
    $genreRepo = new PDOGenreRepository();

    if(count($genreRepo->all()) == 0)
    {
      for($i = 0; $i < count($genres); $i++)
      {

        $genre = new \Models\Genre;
        $genre->name = $genres[$i];

        $genreRepo->create($genre);
      }
    }

  }


  private function createBooks()
  {
    $bookRepo = new PDOBookRepository();

    if(count($bookRepo->all()) == 0)
    {

      for($i = 0; $i < $this->num_of_books; $i++)
      {

        $book = new \Models\Book;
        $book->name = $this->faker->sentence(3);
        $book->pages = $this->faker->numberBetween(200,700);
        $book->image = $this->faker->imageUrl(200, 400, 'cats');
        $book->description = $this->faker->text(400);

        $bookId = $bookRepo->create($book);

      }

      $this->addBooksToSerie();
      $this->addAuthorsToBooks();
      $this->addGenresToBooks();
      $this->addRandomLikes();
    }

  }

  private function createSeries()
  {
    $series = new PDOSerieRepository();

    if (count($series->all()) == 0)
    {
      for ($i=0; $i < $this->num_of_series; $i++) {
        $serie = new \Models\Serie;
        $serie->name = $this->faker->sentence($this->faker->numberBetween(1,3));
        $serie->description = $this->faker->text(400);
        $series->create($serie);
      }
    }
  }

  private function addBooksToSerie()
  {
      $series = new PDOSerieRepository();
      $books = new PDOBookRepository();
      $booksTotal = count($books->all());
      $booksInASerie = [];
      foreach ($series->all() as $serie)
      {
        $booksInThisSerie = $this->faker->numberBetween($this->series_per_book[0],$this->series_per_book[1]);

        while($booksInThisSerie > 0) {
          $b = $this->faker->numberBetween(1, $booksTotal);

          if ( !in_array($b, $booksInASerie)) {
            $booksInASerie[] = $b;
            $series->addBookToSerie($b, $serie->id);

          }
          $booksInThisSerie -= 1;
        }
      }
  }
  private function addGenresToBooks()
  {
    $books = new PDOBookRepository();
    $genres = new PDOGenreRepository();

    $genreCount = count($genres->all());

    foreach ($books->all() as $book)
    {
      $genres_per_book = $this->faker->numberBetween($this->genres_per_book[0], $this->genres_per_book[1]);

      $genresInDataBase = [];
      while($genres_per_book > 0) {
        $newGenre = $this->faker->numberBetween(1, $genreCount);
        if(!in_array($newGenre, $genresInDataBase))
        {
          $genresInDataBase[] = $newGenre;
          $books->addGenreFor($newGenre, $book->id);
        }
        $genres_per_book -= 1;
      }
    }
  }

  private function addAuthorsToBooks()
  {
    $books = new PDOBookRepository();
    $authors = new PDOAuthorRepository();

    $authorsCount = count($authors->all());

    foreach ($books->all() as $book)
    {
      $num_of_authors = $this->faker->numberBetween($this->authors_per_book[0], $this->authors_per_book[1]);
      $authorsInDataBase = [];
      while($num_of_authors > 0)
      {
        $a = $this->faker->numberBetween(1, $authorsCount);
        if(!in_array($a, $authorsInDataBase))
        {
          $authorsInDataBase[] = $a;
          $books->addAuthorFor($a, $book->id);

        }
        $num_of_authors -= 1;
      }
    }
  }
  private function addRandomLikes()
  {
    $books = new PDOBookRepository();
    $users = new PDOUserRepository();
    $allUsers = count($users->all());
    $allBooks = $books->all();

    if ( count($books->findAllLikes()) == 0)
    {
      foreach ($users->all() as $user)
      {
        $likesForThisUser = $this->faker->numberBetween($this->likes_per_user[0], $this->likes_per_user[1]);
        $likedBooks = [];

        while($likesForThisUser > 0)
        {
          $b = $this->faker->numberBetween(1, count($allBooks));
          if(!in_array($b, $likedBooks))
          {
            $likedBooks[] = $b;
            $books->addLikeFor($user->id, $b);

          }
          $likesForThisUser -= 1;
        }
      }
    }
  }

  private function addRandomFriends()
  {

      $repo = new PDOUserRepository();
      $users = $repo->all();

      foreach($users as $user)
      {
        $friendsForUser = $this->faker->numberBetween($this->friends_per_user[0], $this->friends_per_user[1]);
        for($i = 0; $i < $friendsForUser; $i++)
        {
          $friend = $users[$this->faker->numberBetween(0, count($users)-1)];
          $relationExists = false;

          if($friend->id != $user->id)
          {
            if(isset($relations[$user->id]))
            {
              foreach($relations[$user->id] as $rFriendId)
              {
                if($rFriendId == $friend->id)
                {
                  $relationExists = true;
                  break;
                }
              }
            }
          }

          if($relationExists == false)
          {
            $repo->addFriendFor($user->id, $friend->id);
            $repo->addFriendFor($friend->id, $user->id);
            $relations[$user->id][] = $friend->id;
            $relations[$friend->id][] = $user->id;
          } else {
            // $i--;
          }
        }
      }
    }

}
