<?php namespace Database;

/**
 * Serie repository
 * @author Robbert Stevens
 */
class PDOSerieRepository implements \Database\Interfaces\ISerieRepository
{
  private $model = "Models\Serie";

  public function create($model)
  {
    $query = "INSERT INTO serie (name) VALUES (:name)";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":name", $model->name);
    return $stm->execute();
  }

  public function find($id)
  {
    $query = "SELECT * FROM serie WHERE id = :id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $id);
    $stm->execute();

    return $stm->fetchObject($this->model);
  }

  public function all($limit = null, $offset = null)
  {
    $query = "SELECT * FROM serie ";

    if(!is_null($limit))
      $query .= "LIMIT :limit ";

    if(!is_null($offset))
      $query .= "OFFSET :offset";

    $stm = pdo()->prepare($query);

    if(!is_null($limit))
      $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

    if(!is_null($offset))
      $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

    $stm->execute();
    return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);

  }

  public function remove($id)
  {
    $query = "DELETE FROM serie WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $id);
    return $stm->execute();
  }

  public function update($model)
  {
    $query = "UPDATE serie SET name=:name WHERE id=:id";
    $stm = pdo()->prepare($query);
    $stm->bindParam(":id", $model->id);
    $stm->bindParam(":name", $model->name);
    return $stm->execute();

  }

    public function findAllByNameLike($like, $limit = null, $offset = null)
    {
      $query = "SELECT * FROM serie WHERE name LIKE :name ";

      $parameter = '%'.$like.'%';
      if(!is_null($limit))
        $query .= "LIMIT :limit ";

      if(!is_null($offset))
        $query .= "OFFSET :offset";

      $stm = pdo()->prepare($query);

      $stm->bindParam(":name", $parameter);

      if(!is_null($limit))
        $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

      if(!is_null($offset))
        $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

      $stm->execute();

      return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
    }

    public function findByName($name)
    {
      $query = "SELECT * FROM serie WHERE name = :name";
      $stm = pdo()->prepare($query);
      $stm->bindParam(":name", $name);
      $stm->execute();

      return $stm->fetchObject($this->model);
    }

    /**
     * Find serie by book Id
     * @param $id Book id
     * @return Model/Serie
     */
    public function findSerieByBook($id) {
      $query = "SELECT serie.id, name FROM serie JOIN book_serie ON (serie.id = book_serie.serieId) WHERE id = :name";
      $stm = pdo()->prepare($query);
      $stm->bindParam(":name", $name);
      $stm->execute();

      return $stm->fetchObject($this->model);
    }

    /**
     * Adds a book to the serie
     * @param $book book id
     * @param $serie serie id
     */
    public function addBookToSerie($book, $serie)
    {
      $query = "INSERT INTO `book_serie` (`bookId`, `serieId`) VALUES (:book, :serie)";
      $stm = pdo()->prepare($query);
      $stm->bindParam(":book", $book);
      $stm->bindParam(":serie", $serie);
      return $stm->execute();
    }
    /**
     * Removes a book to the serie
     * @param $book book id
     * @param $serie serie id
     */
    public function removeBookFromSerie($book, $serie) {}
}
