<?php namespace Database;

/**
 * Implementation of the IreviewRepository Interfaces
 * @review Jorrit Bakker
 */

 class PDOReviewRepository implements \Database\Interfaces\IReviewRepository
 {
     private $model = "\Models\Review";

     public function create($model)
     {
       print_r($model);

       $query = "INSERT INTO review (`userId`, `bookId`, `date`, `rating`, `title`, `description`) VALUES (:userId, :bookId, NOW(), :rating, :title, :description)";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":userId", $model->userId);
       $stm->bindParam(":bookId", $model->bookId);
      //  $stm->bindParam(":date", $model->date);
       $stm->bindParam(":rating", $model->rating);
       $stm->bindParam(":title", $model->title);
       $stm->bindParam(":description", $model->description);
       $stm->execute();

       $lastId = pdo()->lastInsertId();
       return $lastId;
     }

     public function find($id)
     {
       $query = "SELECT * FROM review WHERE id = :id";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":id", $id);
       $stm->execute();

       $result = $stm->fetchObject($this->model);
       return $result;
     }

     public function all($limit = null, $offset = null)
     {
       $query = "SELECT * FROM review ";

       if(!is_null($limit))
         $query .= "LIMIT :limit ";

       if(!is_null($offset))
         $query .= "OFFSET :offset";

       $stm = pdo()->prepare($query);

       if(!is_null($limit))
         $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

       if(!is_null($offset))
         $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

       $stm->execute();
       return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
     }

     public function remove($id)
     {
       $query = "DELETE FROM review WHERE id = :id";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":id", $id);
       return $stm->execute();
     }

     public function update($model)
     {
       $query = "UPDATE review SET rating=:rating, title=:title, description=:description WHERE id=:id";
       $stm = pdo()->prepare($query);
       $stm->bindParam(":id", $model->id);
       $stm->bindParam(":rating", $model->rating);
       $stm->bindParam(":title", $model->title);
       $stm->bindParam(":description", $model->description);
       return $stm->execute();
     }

     public function findReviewsByBook($id, $limit = null, $offset = null)
     {
          $query = "SELECT * FROM review WHERE bookId =:bookId ORDER BY `id` DESC";

          if(!is_null($limit))
               $query .= "LIMIT :limit ";

          if(!is_null($offset))
               $query .= "OFFSET :offset";


          $stmt = pdo()->prepare($query);
          $stmt->bindParam(":bookId", $id);

          if(!is_null($limit))
               $stmt->bindParam(":limit", $limit);

          if(!is_null($offset))
               $stmt->bindParam(":offset", $offset);

          $stmt->execute();

          return $stmt->fetchAll(\PDO::FETCH_CLASS, $this->model);
     }

     public function findReviewsByUser($id, $limit = null, $offset = null)
     {
          $query = "SELECT * FROM review WHERE userId = :userId ";

          if(!is_null($limit))
            $query .= "LIMIT :limit ";

          if(!is_null($offset))
            $query .= "OFFSET :offset";

          $stm = pdo()->prepare($query);
          $stm->bindParam(":userId", $id);
          if(!is_null($limit))
            $stm->bindParam(":limit", $limit, \PDO::PARAM_INT);

          if(!is_null($offset))
            $stm->bindParam(":offset", $offset, \PDO::PARAM_INT);

          $stm->execute();
          return $stm->fetchAll(\PDO::FETCH_CLASS, $this->model);
     }
 }
