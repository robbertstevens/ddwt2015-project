<?php namespace Database;

/**
 * Some kind of Repository factory
 * @author Robbert Stevens
 */
class RepositoryFactory
{
    /**
     * Returns an implemented IAuthorRepository
     * @return IAuthorRepository
     */
    public static function author()
    {
        return new \Database\PDOAuthorRepository();
    }

    /**
     * Returns an implemented IBookRepository
     * @return IBookRepository
     */
    public static function book()
    {
        return new \Database\PDOBookRepository();
    }

    /**
     * Returns an implemented IGenreRepository
     * @return IGenreRepository
     */
    public static function genre()
    {
        return new \Database\PDOGenreRepository();
    }

    /**
     * Returns an implemented IReviewRepository
     * @return IReviewRepository
     */
    public static function review()
    {
        return new \Database\PDOReviewRepository();
    }

    /**
     * Returns an implemented ISerieRepository
     * @return ISerieRepository
     */
    public static function serie()
    {
        return new \Database\PDOSerieRepository();
    }

    /**
     * Returns an implemented IUserRepository
     * @return IUserRepository
     */
    public static function user()
    {
        return new \Database\PDOUserRepository();
    }
}
