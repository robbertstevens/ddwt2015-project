<?php namespace Database\Interfaces;

/**
* @author Robbert Stevens
*/
interface IBookRepository extends \Database\Interfaces\IRepository
{
  /**
   * Find all books with name like @param
   * @param $like string
   * @return Array Model/Book
   */
  public function findAllByNameLike($like, $limit = null, $offset = null);

  /**
   * Find book by name
   * @param $name string
   * @return Model/Book
   */
  public function findByName($name);

  /**
   * Find all likes for all books
   * @return Array userId and bookId
   */
  public function findAllLikes();
  /**
   * Find books that a user likes
   * @param $id User Id
   * @return Array Model/Book
   */
  public function findLikesByUser($id);

  /**
   * Find books that a user likes
   * @param $id User Id
   * @return Array Model/Book
   */
  public function findNotLikedByUser($id);

  /**
   * Find one like by one User
   * @param $user User Id
   * @param $book Book Id
   * @return boolean
   */
  public function findLikeByUser($user, $book);

  /**
   * Find books written by an author
   * @param $id Author Id
   * @return Array Model/Book
   */
  public function findBooksByAuthor($id);

  /**
   * Find books in a certain genre
   * @param $id Genre Id
   * @return Array Model/Book
   */
  public function findBooksByGenre($id);

  /**
   * Find books in a certain serie
   * @param $id Serie Id
   * @return Array Model/Book
   */
  public function findBooksBySerie($id);

  /**
   * Find books on readlist of User
   * @param $id User Id
   * @return Array Model/Book
   */
   public function findBooksOnReadlistUser($id);
   public function findBookOnReadlistByUser($userId, $bookId);

   /**
    * Finds book read by user
    * @param $id User id
    * @return Array Models/Book
    */
   public function findBooksReadByUser($id);

   /**
    * Find all books read by userId
    * @param $user User id
    * @param $book Book id
    */
   public function findReadByUser($user, $book);

   /**
    * Add a like too a book from a user
    * @param $user User id
    * @param $book Book id
    * @return boolean
    */
   public function addLikeFor($user, $book);

   /**
    * Remove a like form a book by a user
    * @param $user User id
    * @param $book Book id
    * @return boolean
    */
   public function removeLikeFor($userId, $bookId);

   /**
    * Add an author to a book
    * @param $author Author id
    * @param $book Book id
    * @return boolean
    */
   public function addAuthorFor($author, $book);
   /**
    * Reomve an author from a book
    * @param $author Author id
    * @param $book Book id
    * @return boolean
    */
   public function removeAuthorFor($author, $book);

   /**
    * Add a genre to a book
    * @param $author Author id
    * @param $book Book id
    * @return boolean
    */
   public function addGenreFor($genre, $book);

   /**
    * Remove a author from a book
    * @param $genre Genre id
    * @param $book Book id
    * @return boolean
    */
   public function removeGenreFor($genre, $book);

   /**
    * Adds a book to a user's readlist
    * @param $book the book Id
    * @param $user  the user Id
    * @return boolean
    */
   public function addToUserReadlist($user, $book);

   /**
    * Remove from user readlist
    * @param $book the book Id
    * @param $user  the user Id
    * @return boolean
    */
   public function removeFromUserReadlist($user, $book);


   /**
    * Adds a book to a user's readlist
    * @param $book the book Id
    * @param $user  the user Id
    * @return boolean
    */
   public function addReadFor($user, $book);

   /**
    * Remove from user readlist
    * @param $book the book Id
    * @param $user  the user Id
    * @return boolean
    */
   public function removeReadFor($user, $book);

  /**
   * Get total amount of likes of all books together
   * @return integer Total amount of likes
   */
  public function getTotalLikes();

  /**
   * Get total amount of books read for all users
   * @return integer Total amounts of read books
   */
  public function getTotalBooksRead();
}
