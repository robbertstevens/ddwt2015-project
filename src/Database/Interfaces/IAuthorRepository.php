<?php namespace Database\Interfaces;

/**
 * Author repository
 * @author Robbert Stevens
 */
interface IAuthorRepository extends \Database\Interfaces\IRepository
{
    /**
     * Finds Authors by book Id
     * @param $id Book id
     * @return Array Models/Author
     */
    public function findAuthorsByBook($id);

    /**
     * Find all authors by name like the parameter
     * @param $like search parameter
     * @return Array Model/Author
     */
    public function findAllByNameLike($like, $limit = null, $offset = null);
}
