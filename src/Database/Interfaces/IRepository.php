<?php namespace Database\Interfaces;

/**
* Every Repository should implement this interface
* @author Robbert Stevens
*/
interface IRepository
{
  public function create($model);
  public function find($id);
  public function all($limit, $offset);
  public function remove($id);
  public function update($model);
}
