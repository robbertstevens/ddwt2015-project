<?php namespace Database\Interfaces;

interface IUserRepository extends \Database\Interfaces\IRepository
{
  /**
   * Find all users with name like
   * @param $like string
   * @return Array Model/User
   */
  public function findAllByNameLike($like, $limit = null, $offset = null);

  /**
   * Find user with name
   * @param $name string
   * @return Model/User
   */
  public function findByName($name);

 /**
  * Find user with name
  * @param $name string
  * @return Model/User
  */
 public function findByEmail($email);
  /**
   * Returns list of users with based on id
   * @param $id User id
   * @return Array Models/User
   */
  public function findUserFriends($id);

  /**
   * Find all friend request for user X
   * @param $id User id
   * @return Array Models/User
   */
  public function findFriendRequestFor($id);
  /**
   * returns a array with of users that liked book with id
   * @param $id Book id
   * @return Array Models/User
   */
  public function findLikesForBook($id);

  /**
   * Finds a user by email address
   * @param $email string
   * @return Models/User
   */
  public function findUserByEmail($email);

  /**
   * Adds a friend to a user
   * @param $user id,
   * @param $friend id
   * @return boolean
   */
  public function addFriendFor($user, $friend);

  /**
   * Removes friend request
   * @param $user User id
   * @param $friend Friend id
   * @return boolean
   */
  public function declineFriendFor($user, $friend);

}
