<?php namespace Database\Interfaces;

/**
* @author Robbert Stevens
**/

interface IGenreRepository extends \Database\Interfaces\IRepository
{
    /**
     * Find genre by name
     * @param $name string
     * @return Model/Genre
     */
    public function findByName($name);

    /**
     * Find all genres with names like @param
     * @param $like string
     * @return Array Model/Genre
     */
    public function findAllByNameLike($like, $limit = null, $offset = null);

    /**
     * Find all genres by book Id
     * @param $id Book Id
     * @return Array Model/Genre
     */
    public function findGenreByBook($id);

}
