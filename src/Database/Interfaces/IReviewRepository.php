<?php namespace Database\Interfaces;

/**
* @author Jorrit Bakker
*/
interface IReviewRepository extends \Database\Interfaces\IRepository
{
  /**
   * Find all books with name like @param
   * @param $like string
   * @return Array Model/Book
   */


  /**
   * Find book by name
   * @param $name string
   * @return Model/Book
   */
  public function findreviewsByBook($id);
  public function findreviewsByUser($id);

}
