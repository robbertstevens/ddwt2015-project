<?php namespace Database\interfaces;
/**
* The interface for Series
* @author Robbert Stevens
*/
interface ISerieRepository extends IRepository
{
    /**
     * Find all series with name like @param
     * @param $like string
     * @return Array Model/Serie
     */
    public function findAllByNameLike($like, $limit = null, $offset = null);

    /**
     * Find serie by name
     * @param $name string
     * @return Model/Serie
     */
    public function findByName($name);

    /**
     * Find serie by book Id
     * @param $id Book id
     * @return Model/Serie
     */
    public function findSerieByBook($id);

    /**
     * Adds a book to the serie
     * @param $book book id
     * @param $serie serie id
     * @return boolean
     */
    public function addBookToSerie($book, $serie);
    /**
     * Removes a book to the serie
     * @param $book book id
     * @param $serie serie id
     * @return boolean
     */
    public function removeBookFromSerie($book, $serie);
}
