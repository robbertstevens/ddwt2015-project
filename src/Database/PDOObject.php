<?php namespace Database;

/**
 * Makes a object for pdo for easy acces
 * @author Robbert Stevens
 */
class PDOObject
{
    private static $instance;
    private $pdo;
    private function __construct()
    {

      $config = \Util\Config::getInstance();
      $dsn = $config->dsn();
      // $dsn = "sqlite:src/Database/database.db";
      $user = $config->settings["database"]["username"];
      $pass = $config->settings["database"]["password"];

      try
      {
        $this->pdo = new \PDO($dsn, $user, $pass);
      } catch(\Exception $e) {
        print($e->getMessage()); die();
      }
    }

    public function pdo()
    {
      return $this->pdo;
    }

    public static function getInstance()
    {
      if (self::$instance == null)
        return new static();
      else return self::$instance;
    }
}
