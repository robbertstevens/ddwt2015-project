<?php namespace Models;

/**
 * Review Model
 * @author Jorrit Bakker
 */
class Review
{
  public $id;
  public $userId;
  public $bookId;
  public $date;
  public $rating;
  public $title;
  public $description;


  function book(){
    $repo = \Database\RepositoryFactory::book();
    return $repo->find($this->bookId);
  }

  function user(){
    $repo = \Database\RepositoryFactory::user();
    return $repo->find($this->userId);
  }

}
