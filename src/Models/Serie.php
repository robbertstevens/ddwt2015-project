<?php namespace Models;

/**
 * Series Model
 * @author Jorrit Bakker
 * @version 0.1
 */

class Serie
{
  public $id;
  public $name;

  /**
   * @return Array Models/Book
   */
  function books(){
    $repo = \Database\RepositoryFactory::book();
    return $repo->findBooksBySerie($this->id);
  }


}
