<?php namespace Models;

/**
* User Model
* @author Robbert Stevens
* @version 0.1
*/

class User
{
  public $id;
  public $name;
  public $email;
  private $password;
  public $dob;
  public $gender;
  public $oneliner;

  /**
  * password - Sets the password
  * @author Jorrit Bakker
  * @version 0.1
  */
  function password($password)
  {
    $this->password = password_hash($password, PASSWORD_DEFAULT);
  }

  /**
  * password - returns the password
  * @author Jorrit Bakker
  * @version 0.1
  */
  function getPassword()
  {
    return $this->password;
  }

  /**
  * Verifies password and returns a boolean
  * @author Jorrit Bakker
  * @version 0.1
  */
  function checkPassword($input)
  {
    return password_verify($input, $this->password);
  }


  /**
  * returns the friends of a user
  * @author Jorrit Bakker
  * @version 0.1
  */
  function friends()
  {
    $repo = \Database\RepositoryFactory::user();
    return $repo->findUserFriends($this->id);
  }

  function friendRequest()
  {
    $repo = \Database\RepositoryFactory::user();
    return $repo->findFriendRequestFor($this->id);
  }
  
  function hasRequestedFriend($id)
  {

    $users = \Database\RepositoryFactory::user();
    $requests = $users->findFriendRequestFor($this->id);
    foreach ($requests as $friend)
    {
      if ( $friend->id == $id)
        return true;
    }
    return false;
  }
  /**
  * Check if user is friend with other user
  */
  function isFriend($id)
  {
    $users = \Database\RepositoryFactory::user();
    $friends = $users->findUserFriends($this->id);

    foreach ($friends as $friend)
    {
      if ( $friend->id == $id)
      return true;
    }
    return false;
  }
  /**
  * returns a list of liked movies
  * @author Jorrit Bakker
  * @version 0.1
  */
  function likes()
  {
    $repo = \Database\RepositoryFactory::book();
    return $repo->findLikesByUser($this->id);
  }

  /**
  * returns the list of movies in the watchlist
  * @author Jorrit Bakker
  * @version 0.1
  */
  function read()
  {
    $repo = \Database\RepositoryFactory::book();
    return $repo->findBooksReadByUser($this->id);
  }

  /**
  * returns the list of movies in the watchlist
  * @author Jorrit Bakker
  * @version 0.1
  */
  function readlist()
  {
    $repo = \Database\RepositoryFactory::book();
    return $repo->findBooksOnReadlistUser($this->id);
  }


  /**
  * Generate avatar link for gravatar
  * @param $size Image size
  */
  function avatar($size)
  {
    $url = 'http://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $this->email ) ) );
    $url .= "?s=$size&d=wavatar&r=g";

    return $url;
  }
}
