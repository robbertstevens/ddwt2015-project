<?php namespace Models;

/**
 * Film Model
 * @author Robbert Stevens
 */
class Book
{
  public $id;
  public $name;
  public $pages;
  public $image;
  public $description;

  /**
   * @return Array Models/User
   */
	function likes()
	{
  	$repo = \Database\RepositoryFactory::user();
		return $repo->findLikesForBook($this->id);
	}

  /**
   * @return Array Models/Author
   */
  function authors()
  {
    $repo = \Database\RepositoryFactory::author();
		return $repo->findAuthorsByBook($this->id);
  }

  /**
   * @return Array Models/Genre
   */
  function genres()
  {
    $repo = \Database\RepositoryFactory::genre();
		return $repo->findGenreByBook($this->id);
  }

  /**
   * @return Models/Serie
   */
  function serie()
  {
    $repo = \Database\RepositoryFactory::serie();
		return $repo->findSerieByBook($this->id);
  }

  /**
   * @return Models/Serie
   */
  function reviews()
  {
    $repo = \Database\RepositoryFactory::review();
    return $repo->findreviewsByBook($this->id);
  }
}
