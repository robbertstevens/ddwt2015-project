<?php namespace Models;

/**
 * Genre Model
 * @author Jorrit Bakker
 * @version 0.1
 */

class Genre
{
  public $id;
  public $name;

  /**
   * @return Array Models/Book
   */
  function books(){
    $repo = \Database\RepositoryFactory::book();
    return $repo->findBooksByGenre($this->id);
  }

}
