<?php namespace Models;

/**
 * Author Model
 * @author Robbert Stevens
 */
class Author
{
    public $id;
    public $name;
    public $description;

    /**
     * @return Array Models/Book
     */
    public function books() {
        $books = \Database\RepositoryFactory::book();
        return $books->findBooksByAuthor($this->id);
    }
}
