<?php

/**
 * The basis of this application the application class
 * inits all routes, and controls everything in the application
 * @author Robbert Stevens
 * @version 0.1
 */
class Application
{
  private $config;

  /**
   * If everythings goes right this will run the application
   * @return Response
   */
  public function run()
  {
    $router = new Routing\Router($_SERVER["REQUEST_URI"]);
    $route = $router->getRequest();

    if (is_null($route)) {
      return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }
    if ($_SERVER["REQUEST_METHOD"] === $route->method)
      return $this->callController($route->controller, $route->action, $route->matches);
    else {
      return \Util\View::make("Global/405", [], ["HTTTP/1.1 405"]);
    }
  }


  /**
   * Call the controller for the current route
   * @return Response
   */
  public function callController($controller, $method, $args)
  {
    try {
      $class = new \ReflectionClass($controller);
      $method = $class->getMethod($method);

      $method->invokeArgs($class->newInstance(), $args);
    } catch(\ReflectionException $e) {
      echo $e->getMessage();
    }
  }
}
