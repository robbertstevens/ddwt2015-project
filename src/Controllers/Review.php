<?php namespace Controllers;

/**
 * Author Controllers
 * @author Robbert Stevens
 */
class Review
{
    public function show($id)
    {
        $repo = \Database\Repository::review();
        $review = $repo->find($id);
        if ( $review )
            return render("Review/show", ["review" => $review]);
        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);

    }
    public function create()
    {


    }

    public function add()
    {
      $reviews = \Database\RepositoryFactory::review();

      $review = new \Models\Review();
      $review->userId = auth()->id();
      $review->bookId = request()->post->bookId;
      $review->rating = request()->post->rating;
      $review->title = request()->post->title;
      $review->description = request()->post->description;


      $check = new \Util\Validator($review, [
        "rating" => "^(1|2|3|4|5)+$", // Also including accented characters, dont ask me how or why somehting with \p{L} describing the unicode letter classs
        "title" => "^[\w|\W]+$",
        "description" => "^[\w|\W]+$",
      ]);

      if(!$check->validate()) {
        return redirect("book", [":id" => $review->bookId], ["errors" => ["U dient alle velden juist in te vullen"]]);
      }

      $reviews->create($review);
      return redirect("book", [":id" => $review->bookId]);

    }

    public function edit($id)
    {

      $reviews = \Database\RepositoryFactory::review();
      $review = $reviews->find($id);

      if(auth()->id() == $review->userId)
      {
        return render("Review/edit", ["review" => $review]);
      }
      else
      {
        return redirect("book", [":id" => $review->bookId]);
      }
    }

    public function update()
    {
      $reviews = \Database\RepositoryFactory::review();
      $review = $reviews->find(request()->post->id);

      if($review->userId == auth()->id()){

        $review->rating = request()->post->rating;
        $review->title = request()->post->title;
        $review->description = request()->post->description;

        $check = new \Util\Validator($review, [
          "rating" => "^(1|2|3|4|5)+$", // Also including accented characters, dont ask me how or why somehting with \p{L} describing the unicode letter classs
          "title" => "^[\w|\W]+$",
          "description" => "^[\w|\W]+$",
        ]);

        if(!$check->validate()) {
          return redirect("review_update", [":id" => $review->id], ["errors" => ["U dient alle velden juist in te vullen"], "review" => $review]);
        }

        $reviews->update($review);

        return redirect("book", [":id" => $review->bookId]);

      }
      else
      {
        return redirect("book", [":id" => $review->bookId]);
      }
    }


    public function remove($id)
    {
      $reviews = \Database\RepositoryFactory::review();
      $review = $reviews->find($id);


      if($review->userId == auth()->id()){
        $reviews->remove($id);

      }
      return redirect("book", [":id" => $review->bookId]);

    }

    // public function all()
    // {
    //       $reviewRepo = \Database\Repository::review();
    //
    //       $page = request()->get->page ? request()->get->page : 1;
    //
    //       $limit = 5;
    //       $start = ($page - 1) * $limit;
    //
    //       $paginate = new \Util\Paginate($reviewRepo->all(), $page, $limit);
    //       return render("Au/list", ["authors" => $reviewRepo->all($limit, $start), "pagination" => $paginate]);
    // }
}
