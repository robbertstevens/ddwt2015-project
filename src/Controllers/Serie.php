<?php namespace Controllers;

class Serie
{

    public function show($id)
    {
        $series = \Database\RepositoryFactory::serie();
        $serie = $series->find($id);
        if ( $serie )
            return render("Serie/show", ["serie" => $serie]);
        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);

    }
    public function create() {}
    public function remove($id) {}

    public function all()
    {
          $series = \Database\RepositoryFactory::serie();

          $page = request()->get->page ? request()->get->page : 1;

          $limit = 5;
          $start = ($page - 1) * $limit;

          $paginate = new \Util\Paginate($series->all(), $page, $limit);
          return render("Serie/list", ["series" => $series->all($limit, $start), "pagination" => $paginate]);
    }
}
