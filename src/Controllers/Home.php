<?php namespace Controllers;


/**
 * The controller for the home page
 * @author Robbert Stevens
 */
class Home {

  /**
   *
   */
  public function start()
  {
    $users = \Database\RepositoryFactory::user();
    $books = \Database\RepositoryFactory::book();
    $rec = new \Recommender\UserBasedBookRecommender();
    $reviews = \Database\RepositoryFactory::review()->all();
    $page = array(
      "title" => "Lees genot!",
      "description" => "Welkom op de pagina",
      "users" => $users,
      "books" => $books,
      "recommended" => [],
      "reviews" => $reviews
    );

    if (auth()->isLoggedIn())
    {
      $page["recommended"] = limit($rec->getRecommendations(auth()->id()), 5);
    }
    return render("Home/start", $page);
  }

  public function itemBasedBook()
  {
    $before = microtime(true);


    $rec = new \Recommender\ItemBasedBookRecommender();
    $rec->getRecommendations(10);

    $after = microtime(true);
    echo ($after-$before) . " sec/serialize\n";
  }

  public function userBasedBook()
  {
    $before = microtime(true);

    $rec = new \Recommender\UserBasedBookRecommender();

    $rec->getRecommendations(10);

    $after = microtime(true);

    echo ($after-$before) . " sec/\n";
  }

  public function statistics($id)
  {
    set_time_limit(60*15);
    $userRecommender = new \Recommender\UserBasedBookRecommender();
    $itemRecommender = new \Recommender\ItemBasedBookRecommender();


    $before = microtime(true);
    $userRecommendations = $userRecommender->getRecommendations($id);
    $after = microtime(true);

    $userTime = $after-$before;

    $before = microtime(true);
    $itemRecommendations = $itemRecommender->getRecommendations($id);
    $after = microtime(true);

    $itemTime = $after-$before;
    set_time_limit(30);
    return render("Stats/overview", ["userBased" => $userRecommendations,  "userTime" => $userTime,"itemBased" => $itemRecommendations,  "itemTime" => $itemTime, "userId" => $id]);
  }


  public function CSVStatistics($id)
  {
    set_time_limit(60*15);
    $userRecommender = new \Recommender\UserBasedBookRecommender();
    $itemRecommender = new \Recommender\ItemBasedBookRecommender();

    $users = \Database\RepositoryFactory::user();
    $books = \Database\RepositoryFactory::book();

    $numBooks = count($books->all());
    $numUsers = count($users->all());

    $userids = [1, 25, 50, 100];
    header("content-type: text/csv");
    echo "userid;bookid;userbased;itembased;books;users\n";

    foreach($userids as $id){
      $before = microtime(true);
      $userRecommendations = $userRecommender->getRecommendations($id);
      $after = microtime(true);

      $userTime = $after-$before;

      $before = microtime(true);
      $itemRecommendations = $itemRecommender->getRecommendations($id);
      $after = microtime(true);

      $itemTime = $after-$before;


      foreach($userRecommendations as $key => $value)
      {
          echo $id.";".$key.";".$value[0].";".$itemRecommendations[$key][0].";".$numBooks.";".$numUsers."\n";
      }
    }

    set_time_limit(30);
  }


}
