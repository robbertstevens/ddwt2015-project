<?php namespace Controllers;

class Friends
{

    /**
     * Over view of all friends of a user
     * @return Response
     */
    function all($id)
    {
        $users = \Database\RepositoryFactory::user();

        $friends = $users->find($id);
        if ($friends)
            return render("Friends/list", ["user" => $friends]);

        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }

    /**
     * Post page to send a friend request
     * @return Response
     */
    function send()
    {
        if(auth()->isLoggedIn())
        {
            $users = \Database\RepositoryFactory::user();
            $friend = request()->post->friend;
            $users->addFriendFor(auth()->id(), $friend);

            return redirect("user_profile", [":id" => $friend]);
        }

        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }

    /**
     * Page to see friend request for a user
     * @return Response
     */
    function requests()
    {
        if(auth()->isLoggedIn())
        {
            $users = \Database\RepositoryFactory::user();

            $user = $users->find(auth()->id());
            return render("Friends/requests", ["user" => $user]);
        }

        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }

    /**
     * Friend accept post page
     * @return Response
     */
    function accept()
    {
        if(auth()->isLoggedIn())
        {
            $users = \Database\RepositoryFactory::user();
            $friend = request()->post->friend;
            $users->addFriendFor(auth()->id(), $friend);

            return redirect("friend_requests");
        }

        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }

    /**
     * Post page to decline a friendship
     * @return Response
     */
    function decline()
    {
        if(auth()->isLoggedIn())
        {
            $users = \Database\RepositoryFactory::user();
            $friend = request()->post->friend;
            $users->declineFriendFor(auth()->id(), $friend);

            return redirect("friend_requests");
        }

        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }

    /**
     * Friend suggestions page not implemented
     */
    function suggestions()
    {
        echo "suggestions";
    }
}
