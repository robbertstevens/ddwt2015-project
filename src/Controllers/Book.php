<?php namespace Controllers;

/**
 * Book Controllers
 * @author Robbert Stevens
 */
class Book
{
    /**
     * Show the page for one book
     * @return Response either a page or a 404
     */
    public function show($id)
    {
        $books = \Database\RepositoryFactory::book();
        $book = $books->find($id);
        if ( $book )
            return render("Book/show", ["book" => $book]);
        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }

    public function all()
    {
        $books = \Database\RepositoryFactory::book();

        $page = request()->get->page ? request()->get->page : 1;

        $limit = 5;
        $start = ($page - 1) * $limit;

        $paginate = new \Util\Paginate($books->all(), $page, $limit);
        return render("Book/list", ["books" => $books->all($limit, $start), "pagination" => $paginate]);
    }

    /**
     * Route to to like a book
     * @return json
     */
    public function like()
    {
        $books = \Database\RepositoryFactory::book();
        $users = \Database\RepositoryFactory::user();

        $id = request()->post->book_id;
        if($books->find($id)) {
            if(!$books->findLikeByUser(auth()->id(), $id)) {
                $books->addLikeFor(auth()->id(), $id);
                $message = [
                    "message" => "like",
                    "user_id" => auth()->id(),
                    "user_url" => url("user_profile", [":id" => auth()->id()]),
                    "avatar" => $users->find(auth()->id())->avatar(50)
                ];
            }
            else {
                $books->removeLikeFor(auth()->id(), $id);
                $message = [
                    "message" => "unlike",
                    "user_id" => auth()->id()
                ];
            }
            return json($message);
        } else {
            $message = ["message" => "something went wrong"];
            return json($message);
        }
    }

    /**
     * returns the status of the user whenever he likes or dislikes the book
     * @return json if the user lieks or dislikes the book
     */
    public function likeStatus()
    {
        $books = \Database\RepositoryFactory::book();
        $id = request()->post->book_id;
        if($books->find($id)) {
            if(!$books->findLikeByUser(auth()->id(), $id)) {
                $message = ["message" => "like"];
            }
            else {
                $message = ["message" => "unlike"];
            }
            return json($message);
        } else {
            $message = ["message" => "something went wrong"];
            return json($message);
        }
    }


    /**
     * Route to to like a book
     * @return json
     */
    public function readlist()
    {
        $books = \Database\RepositoryFactory::book();
        $users = \Database\RepositoryFactory::user();

        $id = request()->post->book_id;
        if($books->find($id)) {
            if(!$books->findBookOnReadlistByUser(auth()->id(), $id)) {
                $books->addToUserReadlist(auth()->id(), $id);
                $message = [
                    "message" => "onReadlist",
                    "user_id" => auth()->id(),
                ];
            }
            else {
                $books->removeFromUserReadlist(auth()->id(), $id);
                $message = [
                    "message" => "notOnReadlist",
                    "user_id" => auth()->id()
                ];
            }
            return json($message);
        } else {
            $message = ["message" => "something went wrong"];
            return json($message);
        }
    }

    /**
     * returns the status of the user whenever he likes or dislikes the book
     * @return json if the user lieks or dislikes the book
     */
    public function readlistStatus()
    {
        $books = \Database\RepositoryFactory::book();
        $id = request()->post->book_id;
        if($books->find($id)) {
            if(!$books->findBookOnReadlistByUser(auth()->id(), $id)) {
                $message = ["message" => "onReadlist"];
            }
            else {
                $message = ["message" => "notOnReadlist"];
            }
            return json($message);
        } else {
            $message = ["message" => "something went wrong"];
            return json($message);
        }
    }



    /**
     * Route to to like a book
     * @return json
     */
    public function read()
    {
        $books = \Database\RepositoryFactory::book();
        $users = \Database\RepositoryFactory::user();
        $id = request()->post->book_id;
        if($books->find($id)) {
            if(!$books->findReadByUser(auth()->id(), $id)) {
                $books->addReadFor(auth()->id(), $id);
                $message = [
                    "message" => "read",
                    "user_id" => auth()->id(),
                ];
            }
            else {
                $books->removeReadFor(auth()->id(), $id);
                $message = [
                    "message" => "unread",
                    "user_id" => auth()->id()
                ];
            }
            return json($message);
        } else {
            $message = ["message" => "something went wrong"];
            return json($message);
        }
    }

    /**
     * returns the status of the user whenever he likes or dislikes the book
     * @return json if the user lieks or dislikes the book
     */
    public function readStatus()
    {
        $books = \Database\RepositoryFactory::book();
        $id = request()->post->book_id;
        if($books->find($id)) {
            if(!$books->findLikeByUser(auth()->id(), $id)) {
                $message = ["message" => "read"];
            }
            else {
                $message = ["message" => "unread"];
            }
            return json($message);
        } else {
            $message = ["message" => "something went wrong"];
            return json($message);
        }
    }
}
