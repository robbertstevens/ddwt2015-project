<?php namespace Controllers;
/**
 * Search controller
 * @author Robbert Stevens
 */
class Search {

  private $books;
  private $users;
  private $authors;
  private $genres;
  private $series;

  public function __construct() {
      $this->books = \Database\RepositoryFactory::book();
      $this->users = \Database\RepositoryFactory::user();
      $this->authors = \Database\RepositoryFactory::author();
      $this->genres = \Database\RepositoryFactory::genre();
      $this->series = \Database\RepositoryFactory::serie();
  }
  public function search() {
    $input = request()->get->search;
    $limit = 5;

    $bookPage = request()->get->book ? request()->get->book : 1;
    $userPage = request()->get->user ? request()->get->user : 1;
    $authorPage = request()->get->author ? request()->get->author : 1;
    $genrePage = request()->get->genre ? request()->get->genre : 1;
    $seriePage = request()->get->serie ? request()->get->serie : 1;

    $array = array(
      "books" => $this->books->findAllByNameLike($input, $limit, ($bookPage - 1) * $limit),
      "bookPaging" => new \Util\Paginate($this->books->findAllByNameLike($input), $bookPage, $limit),

      "users" => $this->users->findAllByNameLike($input, $limit, ($userPage - 1) * $limit),
      "userPaging" => new \Util\Paginate($this->users->findAllByNameLike($input), $userPage, $limit),

      "authors" => $this->authors->findAllByNameLike($input, $limit, ($authorPage - 1) * $limit),
      "authorPaging" => new \Util\Paginate($this->authors->findAllByNameLike($input), $authorPage, $limit),

      "genres" => $this->genres->findAllByNameLike($input, $limit, ($genrePage - 1) * $limit),
      "genrePaging" => new \Util\Paginate($this->genres->findAllByNameLike($input), $genrePage, $limit),

      "series" => $this->series->findAllByNameLike($input, $limit, ($seriePage - 1) * $limit),
      "seriePaging" => new \Util\Paginate($this->series->findAllByNameLike($input), $seriePage, $limit),
    );

    return render("Search/search", $array);
  }

  public function suggestions() {
    $array = array(
      "books" => $this->books->all(),
      "users" => $this->users->all(),
      "authors" => $this->authors->all(),
      "genres" => $this->genres->all(),
      "series" => $this->series->all(),
    );

    return json($array);
  }
}
