<?php namespace Controllers;

/**
* Book Controllers
* @author Jorrit Bakker
*/
class Genre
{

    public function show($id)
    {
        $genres = \Database\RepositoryFactory::genre();
        $genre = $genres->find($id);

        if($genre)
        {
            $books = \Database\RepositoryFactory::book();

            $page = request()->get->page ? request()->get->page : 1;

            $limit = 5;
            $start = ($page - 1) * $limit;

            $paginate = new \Util\Paginate($genre->books(), $page, $limit);
            return render("Genre/show", [
                "genre" => $genre,
                "books" => $books->findBooksByGenre($genre->id, $limit, $start),
                "pagination" => $paginate
            ]);
        }
        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
    }

    public function create()
    {

    }
    public function remove($id)
    {

    }

    public function all()
    {
        $genres = \Database\RepositoryFactory::genre();

        $page = request()->get->page ? request()->get->page : 1;

        $limit = 5;
        $start = ($page - 1) * $limit;

        $paginate = new \Util\Paginate($genres->all(), $page, $limit);
        return render("Genre/list", ["genres" => $genres->all($limit, $start), "pagination" => $paginate]);
    }

}
