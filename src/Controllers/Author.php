<?php namespace Controllers;

/**
 * Author Controllers
 * @author Robbert Stevens
 */
class Author
{
    public function show($id)
    {
        $authors = \Database\RepositoryFactory::author();
        $author = $authors->find($id);
        if ( $author )
            return render("Author/show", ["author" => $author]);
        return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);

    }
    public function create() {}
    public function remove($id) {}

    public function all()
    {
          $authorRepo = \Database\RepositoryFactory::author();

          $page = request()->get->page ? request()->get->page : 1;

          $limit = 5;
          $start = ($page - 1) * $limit;

          $paginate = new \Util\Paginate($authorRepo->all(), $page, $limit);
          return render("Author/list", ["authors" => $authorRepo->all($limit, $start), "pagination" => $paginate]);
    }
}
