<?php namespace Controllers;

class User {

  function show($id) {
    $users = \Database\RepositoryFactory::user();
    $user = $users->find($id);

    if ($user)
    return render("User/show", ["user" => $user]);

    return \Util\View::make("Global/404", [], ["HTTTP/1.1 404 Not Found"]);
  }

  /**
  * Get route
  * @return View
  */
  public function create()
  {
    return render("User/create");
  }

  /**
  * Post route
  * @return Response
  */
  public function add()
  {
    $users = \Database\RepositoryFactory::user();

    $user = new \Models\User();
    $user->email = request()->post->email;
    $user->name = request()->post->username;
    $user->password(request()->post->password);

    $check = new \Util\Validator($user, [
      "name" => "^[\p{L}\s-]+$", // Also including accented characters, dont ask me how or why somehting with \p{L} describing the unicode letter classs
      "email" => "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
    ]);

    if(!$check->validate()) {
      return redirect("user_create", [], ["errors" => ["Er is iets fout gegaan met het registerern probeer het opnieuw"]]);
    }
    if($users->findByEmail($user->email)) {
      return redirect("user_create", [], ["errors" => ["Er is iets fout gegaan met het registerern probeer het opnieuw"]]);
    }
    $users->create($user);
    auth()->login(request()->post->email, request()->post->password);

    return redirect("home");

  }

  /**
   * Edit page
   * @param $id Id of the user
   */
  public function edit($id)
  {
    $users = \Database\RepositoryFactory::user();
    $user = unserialize(request()->temp->user);

    if ( !$user )
    {
      $user = $users->find($id);
    }

    if(auth()->id() == $id)
    {
      return render("User/edit", ["user" => $user]);
    }
    else
    {
      return redirect("user_profile", [":id" => $id]);
    }
  }

  /**
   * Update the user Database get called in edit method
   */
  public function update()
  {
    $users = \Database\RepositoryFactory::user();

    $id = auth()->id();
    $user = $users->find($id);

    $user->email = request()->post->email;
    $user->name = request()->post->username;
    $user->dob = request()->post->dob;
    $user->gender = request()->post->gender;
    $user->oneliner = request()->post->oneliner;

    $check = new \Util\Validator($user, [
      "name" => "^[\p{L}\s-]+$", // Also including accented characters, dont ask me how or why somehting with \p{L} describing the unicode letter classs
      "dob" => "",
      "gender" => "^(male|female)$",
     ]);

    if(!$check->validate())
    {
      return redirect("user_update", [":id" => $id], ["user" => serialize($user), "errors" => $check->errors]);
    }

    $user = $users->update($user);
    return redirect("user_profile", [":id" => $id]);
  }


  /**
   * The login page post page
   * @return Response
   */
  public function login(){

    $login = new \stdClass();
    $login->email = request()->post->email;
    $login->password = request()->post->password;

    $check = new \Util\Validator($login, [
      "email" => "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
      "password" => "",
    ]);

    if(auth()->login(request()->post->email, request()->post->password)){
      return redirect("user_profile", [":id" => auth()->id()]);
    }
    else
    {
      return redirect("user-login", [], ["errors" => ["Er ging iets verkeerd probeer het opnieuw!"]]);
    }
  }

  /**
   * Renders signin page
   * @return Response
   */
  public function signin()
  {
    return render("User/login");
  }

  /**
   * Log the user out then redirects to home page
   * @return Response
   */
  public function logout()
  {
    auth()->logout();
    return redirect("home");
  }

}
