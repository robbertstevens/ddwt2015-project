<?php namespace Routing;

/**
 * Information about current request
 * @author Robbert Stevens
 */
class Request {
  /**
   * @var the instance
   */
  private static $instance;
  /**
   * @var the requested uri
   */
  public $uri;
  /**
   * @var Request method
   */
  public $method;
  /**
   * @var Post data
   */
  public $post;

  /**
   * @var Query data
   */
  public $get;

  /**
   * @var previous page
   */
  public $referer;


  /**
   * @var temperoraly data
   */
  public $temp;

  private function __construct()
  {

    $this->uri = $_SERVER["REQUEST_URI"];
    $this->method = $_SERVER["REQUEST_METHOD"];
    $this->post = new \Util\MagicArray($_POST);
    $this->get = new \Util\MagicArray($_GET);
    $this->temp = new \Util\MagicArray(isset($_SESSION["TEMP"]) ? $_SESSION["TEMP"] : []);
    $this->route = $this->getRouteName($_SERVER["REQUEST_URI"]);
    $this->referer = (isset($_SERVER["HTTP_REFERER"]) ? $this->getRouteName($_SERVER["HTTP_REFERER"]) : null);
    unset($_SESSION["TEMP"]);
  }

  public static function getInstance()
  {
    if (self::$instance == null)
      return new static();
    else return self::$instance;
  }

  public function getRouteName($route)
  {
    foreach (\Routes::getRoutes() as $name => $r)
    {
      // Create a regular expression based on the params given in the route
      if ( isset($r->params))
        $pattern = strtr($r->pattern, $r->params);
      else
        $pattern = $r->params;
      // this is the real regex
      $regex = '~' . $pattern . '$~';

      if (preg_match($regex, preg_quote($route), $matches)) {
        return $name;
      }
    }
  }
}
