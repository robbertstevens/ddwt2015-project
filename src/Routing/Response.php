<?php namespace Routing;

/**
 * Create a response to sent to the client
 * if extra HTTP headers are specified add them
 */
class Response
{

  function __construct($view, $headers)
  {
    $this->sentHeader($headers);
    $view->loadTemplate();
    exit();
  }

  /**
   * sent the extra HTTP headers, if given
   * @return void
   */
  private function sentHeader($headers)
  {
    foreach($headers as $header)
    {
      header($header);
    }
  }


  /**
   * Make a response
   * @return Response
   */
  public static function make($body, $headers)
  {
    $response = new static($body, $headers);

    return $response;
  }
}
