<?php namespace Routing;

/**
 * A route in the Application
 * @author Robbert Stevens
 */
class Route
{
  /**
   * @var url pattern
   */
  public $pattern;

  /**
   * @var the name of the controller for this route
   */
  public $controller;

  /**
   * @var the method of the controller
   */
  public $action;

  /**
   * @var HTTP request method
   */
  public $method;

  /**
   * @var Array with regex for the pattern
   */
  public $params;

  /**
   * @var Array with matches
   */
  public $matches;
  
  public function __construct($pattern, $controller, $action, $method, $params = array())
  {
    $this->pattern = $pattern;
    $this->controller = $controller;
    $this->action = $action;
    $this->method = $method;
    $this->params = $params;
  }
}
