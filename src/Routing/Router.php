<?php namespace Routing;

/**
 * Router class
 * @author Robbert Stevens
 */
class Router
{

  private $uri;

  private $requestRoute;

  public function __construct($uri)
  {
      $this->uri = parse_url($uri);

      $this->parseRequest();
  }

  public function getRequest()
  {
      return $this->requestRoute;
  }

  /**
   * use the parse_url() fuction of php to parse a given url
   * @return void
   */
  public function parseRequest()
  {
      $matches = array();
      foreach (\Routes::getRoutes() as $route)
      {
        // Create a regular expression based on the params given in the route
        if ( isset($route->params))
          $pattern = strtr($route->pattern, $route->params);
        else
          $pattern = $route->params;

        // this is the real regex
        $regex = '~' . $pattern . '$~';

        if (preg_match($regex, preg_quote($this->uri["path"]), $matches)) {
          array_shift($matches);

          $route->matches = $matches;
          $this->requestRoute = $route;

          if ($_SERVER["REQUEST_METHOD"] === $route->method)
            break;
        }
      }
  }
}
