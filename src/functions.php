<?php
/**
 * Lazy functions to safe keystrokes
 * mostly used for utility classes
 * @author Robbert Stevens
 * @author Jorrit Bakker
 */

/**
 * Creates a views
 */
function render($view, $args = [])
{
  return \Util\View::make($view, $args);
}


/**
 * Creates an URL to specific route
 */
function url($routeName, $args = [], $query = [])
{
  return \Util\Link::toRoute($routeName, $args, $query);
}

/**
 * Json
 */
function json($json)
{
  header('Cache-Control: no-cache, must-revalidate');
  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header('Content-type:application/json;charset=utf-8');
  echo json_encode($json);
}

/**
 * Easy redirect to route
 * @param $routeName the name of the route where to redirect to
 * @param $args arguments for the route
 * @param $temp temperoraly stuff only avaible one request
 */
function redirect($routeName, $args = [], $temp = [])
{
  $redirect = "Location: " . url($routeName, $args);
  $_SESSION["TEMP"] = $temp;

  header($redirect);
}

/**
 * Easy access to Auth class
 * @return instance of Auth class
 */
 function auth()
 {
    return \Util\Auth::getInstance();
 }


/**
 * easy access to the Request classs
 * @return instance of Request class
 */
function request()
{
  return \Routing\Request::getInstance();
}

/**
 * easy acces to Pdo singleton object
 * @return instance of PDOObject
 */
function pdo()
{
  return \Database\PDOObject::getInstance()->pdo();
}

/**
 * Limits a array to the limit
 * @return Array
 */
function limit($array, $limit)
{
  return array_slice($array, 0, $limit, true);
}

function route($name, $route)
{
  \Routes::set($name, $route);
}
