<h1>Auteurs</h1>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Naam</th>
      <th>Aantal boeken</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($authors as $author):  ?>
      <tr>
        <td><a href="<?php echo url("author", [":id" => $author->id]); ?>"><?php echo $author->name; ?></a></td>
        <td><?php echo count($author->books()); ?> boeken</td>
      </tr>
    <?php endforeach; ?>

  </tbody>
</table>
<?php if ($pagination): ?>
  <ul class="pagination pagination-sm">
    <?php if (!is_null($pagination->prev)): ?>
      <li class="previous"><a href="<?php echo url("authors"); ?>?page=<?php echo $pagination->prev; ?>">Vorige</a></li>
    <?php endif; ?>

    <?php foreach($pagination->links() as $link): ?>
      <li <?php echo $link == $pagination->current ? "class=active": ""; ?>>
        <a href="<?php echo url("authors"); ?>?page=<?php echo $link; ?>"><?php echo $link; ?></a>
      </li>
    <?php endforeach; ?>

    <?php if (!is_null($pagination->next)): ?>
      <li class="next"><a href="<?php echo url("authors"); ?>?page=<?php echo $pagination->next; ?>">Volgende</a></li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
