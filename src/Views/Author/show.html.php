<div class="jumbotron">
      <h1><?php echo $author->name; ?></h1>
      <p>
          <?php echo $author->description; ?>
      </p>
</div>

<div class="row">
      <div class="col-sm-12">
            <?php if ($author->books()): ?>
                  <h1>Deze autheur heeft deze boeken geschreven: </h1>
                  <ul>
                        <?php foreach ($author->books() as $book): ?>
                              <li><a href="<?php echo url("book", [":id" => $book->id]); ?>"><?php echo $book->name ?></a></li>
                        <?php endforeach; ?>
                  </ul>
            <?php endif; ?>
      </div>
</div>
