<div class="jumbotron">
  <h1><?php echo $title; ?></h1>
  <p>
    <?php echo $description; ?>
  </p>
</div>

<div class="row">

  <div class="col-sm-4">
    <h2>Statistieken</h2>
    <table class="table">
      <tr>
        <td>Boeken</td>
        <td><?php echo count($books); ?></td>
      </tr>
      <tr>
        <td>Recenties</td>
        <td><?php echo count($reviews) ?></td>
      </tr>
      <tr>
        <td>Likes</td>
        <td><?php echo $books->getTotalLikes(); ?></td>
      </tr>
      <tr>
        <td>Gelezen boeken</td>
        <td><?php echo $books->getTotalBooksRead(); ?></td>
      </tr>
      <tr>
        <td>Leden</td>
        <td><?php echo count($users); ?></td>
      </tr>
    </table>
  </div>
  <div class="col-sm-4">
    <h2>Laatste recensie</h2>
    <?php if ($reviews): ?>
      <ul>
      <?php foreach (limit($reviews, 5) as $review): ?>
        <li>
          <a href="<?php echo url("book", [":id" => $review->book()->id]); ?>">
            <?php echo $review->title ?>
          </a> by
          <a href="<?php echo url("user_profile", [":id" => $review->user()->id]); ?>">
            <?php echo $review->user()->name ?>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
    <?php else: ?>
        <p>
          Nog geen recensies geplaatst
        </p>
    <?php endif; ?>
  </div>
  <div class="col-sm-4">
    <h2>Aanbevelingen</h2>
    <?php if ($recommended): ?>
      <ul>

      <?php foreach ($recommended as $book): ?>
        <li><a href="<?php echo url("book", [":id" => $book[1]->id]); ?>"><?php echo $book[1]->name ?></a> </li>
      <?php endforeach; ?>
    </ul>
    <?php else: ?>
        <p>
          Login om onze aanbevelingen te zien
        </p>
    <?php endif; ?>
  </div>
</div>
