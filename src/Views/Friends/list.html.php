<h1>Vrienden van <a href="<?php echo url("user_profile", [":id"=>$user->id]); ?>"><?php echo $user->name; ?></a></h1>
<?php if (count($user->friends()) > 0): ?>
    <?php foreach ($user->friends() as $friend): ?>
        <a href="<?php echo url("user_profile", [":id" => $friend->id]); ?>"><img src="<?php echo $friend->avatar(50); ?>" alt="<?php echo $friend->name; ?>" /></a>
    <?php endforeach; ?>
<?php endif; ?>
