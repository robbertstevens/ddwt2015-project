<h1>Vriendschapsverzoeken</h1>

<?php if ($user->friendRequest()): ?>
    <table class="table table-striped">
        <tbody>
            <?php foreach ($user->friendRequest() as $friend): ?>
                <tr>
                    <td>
                        <a href="<?php echo url("user_profile", [":id" => $friend->id]) ?>">
                            <?php echo $friend->name ?>
                        </a>
                    </td>
                    <td>
                        <form action="<?php echo url("friend_request_decline"); ?>" method="post">
                            <input type="hidden" name="friend" value="<?php echo $friend->id; ?>">
                            <input type="submit" name="name" value="decline">
                        </form>
                    </td>
                    <td>
                        <form action="<?php echo url("friend_request_accept"); ?>" method="post">
                            <input type="hidden" name="friend" value="<?php echo $friend->id; ?>">
                            <input type="submit" name="name" value="accept">
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <p>
        Geen vriendschapsverzoeken op dit moment
    </p>
<?php endif; ?>
