<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($title) ? $title : "Leesgenot"); ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!--<link rel="stylesheet" href="/public/css/superhero.min.css">-->
    <link rel="stylesheet" href="https://bootswatch.com/journal/bootstrap.min.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/public/css/easy-autocomplete.css" charset="utf-8">
    <link rel="stylesheet" href="/public/css/easy-autocomplete.themes.min.css" charset="utf-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="/public/js/jquery.easy-autocomplete.min.js"></script>
    <script src="/public/js/app.js"></script>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo url('home'); ?> ">Leesgenot</a>
            </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href=".<?php echo \Util\Link::toRoute("home");?>">Home</span></a></li>
              <li><a href="<?php echo \Util\Link::toRoute("books");?>">Boeken</a></li>
              <li><a href="<?php echo \Util\Link::toRoute("authors");?>">Auteurs</a></li>
              <li><a href="<?php echo \Util\Link::toRoute("genres");?>">Genres</a></li>
              <li><a href="<?php echo \Util\Link::toRoute("series");?>">Series</a></li>

            </ul>

            <form class="navbar-form navbar-left" action="<?php echo url("search"); ?>" method="get" role="search">
              <div class="form-group">
                <input type="text" name="search" class="form-control" id="film-search" placeholder="Titel, genre, serie, gebruikers">
              </div>
              <button type="submit" class="btn btn-default">Zoeken</button>
            </form>

            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <?php if(auth()->isLoggedIn()): ?>
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Instellingen<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo url("user_profile", [":id" => auth()->id()]);?>">Profiel</a></li>
                    <li>
                      <a href="<?php echo url("friend_requests", [":id" => auth()->id()]);?>">
                        Verzoeken
                        <?php if (count(auth()->user()->friendRequest()) > 0): ?>
                          <span class="badge">
                            <?php echo count(auth()->user()->friendRequest()) ?>
                          </span>
                        <?php endif; ?>
                      </a>
                    </li>
                    <li><a href="<?php echo url("friends", [":id" => auth()->id()]);?>">Vrienden</a></li>
                    <li><a href="<?php echo url("user_logout");?>">Uitloggen</a></li>
                  </ul>
                <?php else: ?>
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login<span class="caret"></span></a>
                  <ul class="dropdown-menu" style="padding: 15px;">
                    <li>
                      <form action="<?php echo url("user-login-action");?>" method="post" style="width: 200px;">
                        <div class="form-group">
                            <label for="email" class="control-label">Email:</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" data-error="Ongeldig email adres" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                          <label for="password" class="control-label">Wachtwoord:</label>
                          <input type="password" name="password" class="form-control" id="password" placeholder="wachtwoord" data-error="Uw dient een wachtwoord op te geven" required>
                          <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                          <input type="submit" name="name" value="Login" class="btn btn-block"/>
                        </div>
                      </form>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a href="<?php echo url("user_create");?>" style="text-align: center;">Register</a></li>
                  </ul>
                <?php endif; ?>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
