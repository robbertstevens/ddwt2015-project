<h1>Userbased</h1>
<p>
    recommended for user <?php echo $userId; ?> in <?php echo $userTime ?> seconds:
    <?php foreach ($userBased as $key => $value): ?>
        <?php echo $value[1]->id; ?>
        (<?php echo round($value[0], 4); ?>)
    <?php endforeach; ?>
    <br />
    <?php echo count($userBased)." recommendations"; ?>
    <br />
    <?php
    $sum = array_sum(array_map(function($item) {
      return $item[0];
    }, $userBased));
    print("Gemiddelde simalarity: ".$sum/count($userBased));
    ?>
</p>

<h1>Itembased</h1>
<p>
    recommended for user <?php echo $userId; ?> in <?php echo $itemTime ?> seconds:
    <?php foreach ($itemBased as $value): ?>
        <?php echo $value[1]->id; ?>
        (<?php echo round($value[0], 4); ?>)
    <?php endforeach; ?>
    <br />
    <?php echo count($itemBased)." recommendations"; ?>
    <br />
    <?php
    $sum = array_sum(array_map(function($item) {
      return $item[0];
    }, $itemBased));
    print("Gemiddelde simalarity: ".$sum/count($itemBased));
    ?>
</p>


<h1>Overview</h1>
<table class="table">
    <thead>
        <th>
            Book
        </th>
        <th>
            Position
        </th>
        <th>
            UserBased
        </th>
        <th>
            ItemBased
        </th>
        <th>
            Position
        </th>
        <th>
            Difference
        </th>

    </thead>
    <tbody>
        <?php foreach (limit($userBased, 10) as $key => $value): ?>
            <tr>
                <td>
                    <?php echo $key ?>
                </td>
                <td>
                    <?php echo array_search($key, array_keys($userBased)); ?>
                </td>
                <td>
                    <?php echo $value[0] ?>
                </td>
                <td>
                    <?php echo @$itemBased[$key][0]; ?>
                </td>
                <td>
                    <?php echo array_search($key, array_keys($itemBased)); ?>
                </td>
                <td>
                    <strong><?php echo $value[0]-$itemBased[$key][0]; ?></strong>
                </td>

            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<h1>Overview 2</h1>
<table class="table">
    <thead>
        <th>
            Book
        </th>
        <th>
            Position
        </th>
        <th>
            ItemBased
        </th>
        <th>
            UserBased
        </th>
        <th>
            Position
        </th>
        <th>
            Difference
        </th>
    </thead>
    <tbody>
        <?php foreach (limit($itemBased, 10) as $key => $value): ?>
            <tr>
                <td>
                    <?php echo $key ?>
                </td>
                <td>
                    <?php echo array_search($key, array_keys($itemBased)); ?>
                </td>
                <td>
                    <?php echo $value[0] ?>
                </td>
                <td>
                    <?php echo @$userBased[$key][0]; ?>
                </td>
                <td>
                    <?php echo array_search($key, array_keys($userBased)); ?>
                </td>
                <td>
                    <strong><?php echo $value[0]-$userBased[$key][0]; ?></strong>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
