<h1>Boeken in het genre:  <?php echo $genre->name; ?></h1>
<?php if ($books): ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Naam</th>
                <th>Beschrijving</th>
                <th>Bladzijdes</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($books as $book):  ?>
                <tr>
                    <td><a href="<?php echo url("book", [":id" => $book->id]); ?> "><?php echo $book->name; ?></a></td>
                    <td><?php echo $book->description; ?></td>
                    <td><?php echo $book->pages; ?> bladzijdes</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php if ($pagination): ?>
        <ul class="pagination pagination-sm">
            <?php if (!is_null($pagination->prev)): ?>
                <li class="previous"><a href="<?php echo url("genre", [":id" => $genre->id]); ?>?page=<?php echo $pagination->prev; ?>">Vorige</a></li>
            <?php endif; ?>

            <?php foreach($pagination->links() as $link): ?>
                <li <?php echo $link == $pagination->current ? "class=active": ""; ?>>
                    <a href="<?php echo url("genre", [":id" => $genre->id]); ?>?page=<?php echo $link; ?>"><?php echo $link; ?></a>
                </li>
            <?php endforeach; ?>

            <?php if (!is_null($pagination->next)): ?>
                <li class="next"><a href="<?php echo url("genre", [":id" => $genre->id]); ?>?page=<?php echo $pagination->next; ?>">Volgende</a></li>
            <?php endif; ?>
        </ul>
    <?php endif; ?>
<?php else: ?>
    <p>
        Nog geen boeken in dit genre
    </p>
<?php endif; ?>
