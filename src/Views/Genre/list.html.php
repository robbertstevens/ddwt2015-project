<h1>Genres</h1>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Naam</th>
      <th>Boeken</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($genres as $genre):  ?>
      <tr>
        <td><a href="<?php echo url("genre", [":id" => $genre->id]); ?> "><?php echo $genre->name; ?></a></td>
        <td><?php echo count($genre->books());  ?> </td>
      </tr>
    <?php endforeach; ?>

  </tbody>
</table>
<?php if ($pagination): ?>
  <ul class="pagination pagination-sm">
    <?php if (!is_null($pagination->prev)): ?>
      <li class="previous"><a href="<?php echo url("genres"); ?>?page=<?php echo $pagination->prev; ?>">Vorige</a></li>
    <?php endif; ?>

    <?php foreach($pagination->links() as $link): ?>
      <li <?php echo $link == $pagination->current ? "class=active": ""; ?>>
        <a href="<?php echo url("genres"); ?>?page=<?php echo $link; ?>"><?php echo $link; ?></a>
      </li>
    <?php endforeach; ?>

    <?php if (!is_null($pagination->next)): ?>
      <li class="next"><a href="<?php echo url("genres"); ?>?page=<?php echo $pagination->next; ?>">Volgende</a></li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
