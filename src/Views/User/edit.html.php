<div class="jumbotron">
  	<h1><?php echo $user->name ?></h1>
    <p>You're free to edit your profile now!</p>
</div>

<div class="col-lg-3 col-md-6 col-sm-12">
  <h1>Profiel wijzigen</h1>
  <form class="" action="<?php echo url("user_update_post");?>" method="post">
    <div class="form-group">
        <label for="email" class="control-label">Email:</label>
        <input type="email" name="email" class="form-control" id="email" placeholder="Email" disabled value="<?php echo $user->email;?>" data-error="Ongeldig email adres" required>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
      <label for="username" class="control-label">Naam:</label>
      <input type="text" name="username" class="form-control" id="username" pattern="^[a-zA-Z\s]+$" placeholder="Naam"  value="<?php echo $user->name;?>" data-error="Uw naam mag alleen letters bevatten" required>
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
        <label for="gender" class="control-label">Geslacht:</label>
        <select name="gender" class="form-control" id="geslacht">
          <option value="" <?php echo ($user->gender != "male" && $user->gender != "female" ? "selected" : "");?>>Selecteer</option>
          <option value="male" <?php echo ($user->gender == "male" ? "selected" : "");?>>Man</option>
          <option value="female" <?php echo ($user->gender == "female" ? "selected" : "");?>>Vrouw</option>
        </select>
  </div>
    <div class="form-group">
      <label for="dob" class="control-label">Geboortedatum:</label>
      <div class="input-group date" data-provide="datepicker" id="dob">
          <input type="text" name="dob" placeholder="YYYY-MM-DD" pattern="^[0-9]{2}/[0-9]{2}/[0-9]{4}+$" class="form-control"  value="<?php echo $user->dob;?>">
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
      </div>
    </div>
    <div class="form-group">
      <label for="oneliner" class="control-label">Oneliner:</label>
      <input type="text" name="oneliner" class="form-control" id="oneliner" placeholder="Oneliner"  value="<?php echo $user->oneliner;?>">
      <div class="help-block with-errors"></div>
    </div>
    <div class="form-group">
      <input type="submit" name="save" value="Opslaan" class="btn btn-default"/>
    </div>
  </form>
</div>
