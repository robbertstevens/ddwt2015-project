<div class="row">
  <?php if ($request->temp->errors): ?>
    <div class="alert alert-danger" role="alert">
      <?php foreach ($request->temp->errors as $error): ?>
        <p>
          <?php echo $error; ?>
        </p>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
  <div class="col-sm-6 col-md-4 col-md-offset-4">
    <h1>Register</h1>
    <form data-toggle="validator" role="form" action="<?php echo url("user_create_post");?>" method="post">
      <div class="form-group">
        <label for="email" class="control-label">Email:</label>
        <input type="email" name="email" class="form-control" id="email" placeholder="Email" data-error="Ongeldig email adres" required>
        <div class="help-block with-errors"></div>
      </div>
      <div class="form-group">
        <label for="username" class="control-label">Naam:</label>
        <input type="text" name="username" class="form-control" id="username" pattern="^[a-zA-Z\s-]+$" placeholder="Naam" data-error="Uw naam mag alleen letters bevatten" required>
        <div class="help-block with-errors"></div>
      </div>
      <div class="form-group">
        <label for="password" class="control-label">Wachtwoord:</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="wachtwoord" data-error="Uw dient een wachtwoord op te geven" required>
        <div class="help-block with-errors"></div>
      </div>
      <div class="form-group">
        <input type="submit" name="name" class="btn btn-default" value="Registreer" />
      </div>
    </form>
  </div>
</div>
