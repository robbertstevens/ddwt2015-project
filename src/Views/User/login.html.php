<div class="row">
  <?php if ($request->temp->errors): ?>
    <div class="alert alert-danger" role="alert">
      <?php foreach ($request->temp->errors as $error): ?>
        <p>
          <?php echo $error ?>
        </p>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
  <div class="col-sm-6 col-md-4 col-md-offset-4">
      <form action="<?php echo url("user-login-action");?>" method="post">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <a href="<?php echo url("user_create");?>" class="text-center new-account">Registreer</a>
    </div>

</div>
