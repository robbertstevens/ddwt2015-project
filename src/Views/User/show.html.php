<div class="jumbotron">
	<div class="row">
		<div class="col-sm-2">
			<img src="<?php echo $user->avatar(100); ?>" alt="" />
			<p><small><?php echo $user->dob;?></small></p>
		</div>
		<div class="col-sm-9">
			<h1><?php echo $user->name;?> 	<span class="fa <?php echo ($user->gender == "male" ? "fa-mars" : ($user->gender == "female" ? "fa-venus" : "fa-venus-mars"));?>"></span></h1>
			<p><?php echo $user->oneliner;?></p>
			<?php if (auth()->id() == $user->id): ?>
				<a href="<?php echo url("user_update", [":id" => $user->id]);?>">Wijzig profiel</a>
			<?php else: ?>
				<?php if (!$user->isFriend(auth()->id()) && !$user->hasRequestedFriend(auth()->id())): ?>
					<form action="<?php echo url("friend_request_send"); ?>" method="post">
						<input type="hidden" name="friend" value="<?php echo $user->id; ?>">
						<input type="submit" name="name" value="Toevoegen als vriend">
					</form>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">

		<h2>Vrienden</h2>
		<?php if ($user->friends()): ?>
			<?php foreach (limit($user->friends(), 9) as $friend): ?>
				<a href="<?php echo url("user_profile", [":id" => $friend->id])?>"><img src="<?php echo $friend->avatar(50)?>" alt="" /></a>
			<?php endforeach; ?>
			<?php if (count($user->friends()) > 9 ): ?>
				<a href="<?php echo url("friends", [":id" => $user->id]); ?>">
					<img src="/public/images/more.png" alt="" />
				</a>
			<?php endif; ?>
			<?php else: ?>
				<p>
					Nog geen vrienden toegevoegd.
				</p>
		<?php endif; ?>

		<h2>Likes</h2>
		<?php if ($user->likes()): ?>
			<ul class="list-inline">
				<?php foreach ($user->likes() as $like): ?>
					<li><a href="<?php echo url("book", [":id" => $like->id]); ?>"><?php echo $like->name ?></a></li>
				<?php endforeach; ?>
			</ul>
			<?php else: ?>
				<p>
					Nog geen boeken geliked.
				</p>
		<?php endif; ?>
	</div>
	<div class="col-sm-6">
		<h2>Leeslijst</h2>
		<?php if ($user->readlist()): ?>
			<ul class="list-inline">
				<?php foreach ($user->readlist() as $readlist): ?>
					<li><a href="<?php echo url("book", [":id" => $readlist->id]);?>"><?php echo $readlist->name ?></a></li>
				<?php endforeach; ?>
			</ul>
		<?php else: ?>
			<p>
				Nog geen boeken toegevoegd aan de leeslijst.
			</p>
		<?php endif; ?>
		<h2>Gelezen</h2>

		<?php if ($user->read()): ?>
			<ul class="list-inline">
				<?php foreach ($user->read() as $read): ?>
					<li><a href="<?php echo url("book", [":id" => $read->id]);?>"><?php echo $read->name ?></a></li>
				<?php endforeach; ?>
			</ul>
		<?php else: ?>
			<p>
				Nog geen boeken gelezen.
			</p>
		<?php endif; ?>
	</div>
</div>
