<div class="jumbotron">
  <div class="row">
    <div class="col-sm-3">
      <img src="<?php echo $book->image;?>">
    </div>

    <div class="col-sm-8">
      <h1><?php echo $book->name; ?></h1>
      <p>
        <?php echo $book->description; ?>
      </p>
      <?php if (auth()->isLoggedIn()): ?>
        <a id="like-button" href="<?php echo url("book_like"); ?>" data-book-id="<?php echo $book->id; ?>"=""></a><br />
        <a id="readlist-button" href="<?php echo url("book_readlist"); ?>" data-book-id="<?php echo $book->id; ?>"=""></a><br />
        <a id="read-button" href="<?php echo url("book_read"); ?>" data-book-id="<?php echo $book->id; ?>"=""></a>
      <?php endif; ?>
    </div>

  </div>
</div>

<div class="row">
  <div class="col-sm-6">
    <h1>Liked by</h1>
    <div id="likes">
      <?php if (count($book->likes()) > 0): ?>
        <?php foreach ($book->likes() as $user): ?>
          <a data-id="<?php echo $user->id; ?>" href="<?php echo url("user_profile", [":id" => $user->id]); ?>">
            <img class="img-circle" src="<?php echo $user->avatar(50); ?>" alt="<?php echo $user->name; ?>"/></a>
        <?php endforeach; ?>
      <?php else: ?>
        <p>
          Niemand heeft dit boek nog geliked
        </p>
      <?php endif; ?>
    </div>

    <h1>Genres</h1>
    <?php if (count($book->genres()) > 0): ?>
      <?php foreach ($book->genres() as $genre): ?>
         <a href="<?php echo url("genre", [":id" => $genre->id ]); ?>"><?php echo $genre->name; ?></a> ,
      <?php endforeach; ?>
    <?php else: ?>
      <p>
        Aan dit boek zijn nog genres toegevoegd
      </p>
    <?php endif; ?>

    <h1>Auteurs</h1>
    <?php if (count($book->authors()) > 0): ?>
      <?php foreach ($book->authors() as $author): ?>
        <a href="<?php echo url("author", [":id" => $author->id ]); ?>"><?php echo $author->name; ?></a> ,
      <?php endforeach; ?>
    <?php else: ?>
      <p>
        Dit boek heeft geen bekende auteurs
      </p>
    <?php endif; ?>
  </div>
  <div class="col-sm-6">
    <h1>Recenties</h1>

    <?php if (auth()->isLoggedIn()): ?>
      <form method="post" action="<?php echo url("review_create_post");?>">
        <input type="hidden" name="bookId" value="<?php echo $book->id;?>"/>
        <div class="form-group">
          <strong>Cijfer</strong>
          <label class="radio-inline"><input type="radio" name="rating" value="1">1</label>
          <label class="radio-inline"><input type="radio" name="rating" value="2">2</label>
          <label class="radio-inline"><input type="radio" name="rating" value="3">3</label>
          <label class="radio-inline"><input type="radio" name="rating" value="4">4</label>
          <label class="radio-inline"><input type="radio" name="rating" value="5">5</label>
        </div>

        <div class="form-group">
          <label for="title">Titel</label>
          <input type="text" name="title" value="" class="form-control"/>
        </div>
        <div class="form-group">
          <label for="description">Review</label>
          <textarea name="description" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary form-control">Plaats</button>
        </div>
      </form>
    <?php endif; ?>

    <?php if (count($book->reviews()) > 0): ?>
      <ul class="list-group">
      <?php foreach ($book->reviews() as $review): ?>
        <li class="list-group-item">
          <article class="row">
              <div class="col-md-2 col-sm-2 hidden-xs">
                <figure class="thumbnail">
                  <img class="img-responsive" src="<?php echo $review->user()->avatar(50); ?>" />
                  <figcaption class="text-center">
                    <a href="<?php echo url("user_profile", [":id"=> $review->user()->id]); ?>">
                      <?php echo $review->user()->name; ?>
                    </a>
                  </figcaption>
                </figure>
              </div>
              <div class="col-md-10 col-sm-10">
                <div class="panel panel-default arrow left">
                  <div class="panel-body">
                    <header class="text-left">
                      <div class="comment-user"><?php echo $review->title ?></div>
                      <time class="comment-date" datetime="<?php echo $review->date ?>"><span class="glyphicon glyphicon-time"></span> <?php echo $review->date ?> </time>
                      <span class="glyphicon glyphicon-star"></span><?php echo $review->rating ?>
                    </header>
                    <div class="comment-post">
                      <p>
                        <?php echo $review->description; ?>
                      </p>
                      <?php if($review->userId == auth()->id()): ?>
                        <a href="<?php echo url("review_update", [":id" => $review->id]); ?>" class="btn btn-primary btn-xs">Wijzigen</a>
                      <?php endif;?>
                    </div>
                  </div>
                </div>
              </div>
            </article>
        </li>
      <?php endforeach; ?>
      </ul>
    <?php else: ?>
      <p>
        Dit boek heeft nog geen reviews.
      </p>
    <?php endif; ?>
  </div>
</div>
