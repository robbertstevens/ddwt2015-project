
<form method="post" action="<?php echo url("review_update_post");?>" class="form">
  <input type="hidden" name="id" value="<?php echo $review->id;?>"/>
  <div class="form-group">
    <strong>Cijfer</strong>
    <label class="radio-inline"><input type="radio" name="rating" value="1">1</label>
    <label class="radio-inline"><input type="radio" name="rating" value="2">2</label>
    <label class="radio-inline"><input type="radio" name="rating" value="3">3</label>
    <label class="radio-inline"><input type="radio" name="rating" value="4">4</label>
    <label class="radio-inline"><input type="radio" name="rating" value="5">5</label>
  </div>

  <div class="form-group">
    <label for="title">Titel</label>
    <input type="text" name="title" id="title" value="<?php echo $review->title;?>" class="form-control"  />
  </div>
  <div class="form-group">
    <label for="description">Review</label>
    <textarea name="description" id="description" class="form-control"><?php echo $review->description;?></textarea>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary">Wijzig</button>
    <a href="<?php echo url("review_delete", [":id" => $review->id]);?>" class="link">Verwijderen</a>
  </div>
</form>
