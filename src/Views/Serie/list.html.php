<h1>Series</h1>

<?php if (count($series) > 0): ?>
    <table  class="table table-striped">
        <thead>
            <tr>
                <th> Naam </th>
                <th> Aantal boeken</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($series as $serie): ?>
                <tr>
                    <td>
                        <a href="<?php echo url("serie", [":id" => $serie->id]); ?>">
                            <?php echo $serie->name;?>
                        </a>
                    </td>
                    <td>
                        <?php echo count($serie->books()); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php if ($pagination): ?>
      <ul class="pagination pagination-sm">
        <?php if (!is_null($pagination->prev)): ?>
          <li class="previous"><a href="<?php echo url("series"); ?>?page=<?php echo $pagination->prev; ?>">Vorige</a></li>
        <?php endif; ?>

        <?php foreach($pagination->links() as $link): ?>
          <li <?php echo $link == $pagination->current ? "class=active": ""; ?>>
            <a href="<?php echo url("series"); ?>?page=<?php echo $link; ?>"><?php echo $link; ?></a>
          </li>
        <?php endforeach; ?>

        <?php if (!is_null($pagination->next)): ?>
          <li class="next"><a href="<?php echo url("series"); ?>?page=<?php echo $pagination->next; ?>">Volgende</a></li>
        <?php endif; ?>
      </ul>
    <?php endif; ?>
<?php else: ?>
    <p>
        Nog geen series bekend
    </p>
<?php endif; ?>
