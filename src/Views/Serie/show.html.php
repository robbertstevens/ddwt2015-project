<h1><?php echo $serie->name; ?></h1>
<p>
    <?php echo @$serie->description; ?>
</p>
<?php if (count($serie->books()) > 0): ?>
    <?php foreach ($serie->books() as $book): ?>
        <a href="<?php echo url("book", [":id" => $book->id]); ?>">
            <img src="<?php echo $book->image; ?>" alt="" />
        </a>
    <?php endforeach; ?>
<?php endif; ?>
