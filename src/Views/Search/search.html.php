<h1>Zoek resultaten</h1>
<?php if (count($books) > 0): ?>
    <?php include("_books.html.php"); ?>
<?php endif; ?>

<?php if (count($authors) > 0): ?>
    <?php include("_authors.html.php"); ?>
<?php endif; ?>

<?php if (count($genres) > 0): ?>
    <?php include("_genres.html.php"); ?>
<?php endif; ?>

<?php if (count($series) > 0): ?>
    <?php include("_series.html.php"); ?>
<?php endif; ?>

<?php if (count($users) > 0): ?>
    <?php include("_users.html.php"); ?>
<?php endif; ?>
