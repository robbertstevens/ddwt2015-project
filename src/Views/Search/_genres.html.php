<h2>Genres</h2>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Naam</th>
      <th>Boeken</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($genres as $genre):  ?>
      <tr>
        <td><a href="<?php echo \Util\Link::toRoute("genre", [":id"=> $genre->id]); ?> "><?php echo $genre->name; ?></a></td>
        <td><?php //echo count($genre->books()); ?></a></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php if ($genrePaging): ?>
  <ul class="pagination pagination-sm">
    <?php if (!is_null($genrePaging->prev)): ?>
      <li class="previous">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->prev, "serie" =>  $seriePaging->current]); ?>">
          Vorige
        </a>
      </li>
    <?php endif; ?>

    <?php if (count($genrePaging->links()) > 1): ?>
      <?php foreach($genrePaging->links() as $link): ?>
        <li <?php echo $link == $genrePaging->current ? "class=active": ""; ?>>
          <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $link, "serie" =>  $seriePaging->current]); ?>">
            <?php echo $link; ?>
          </a>
        </li>
      <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!is_null($genrePaging->next)): ?>
      <li class="next">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->next, "serie" =>  $seriePaging->current]); ?>">
          Volgende
        </a>
      </li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
