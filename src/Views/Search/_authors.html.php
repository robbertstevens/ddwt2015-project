<h2>Auteurs</h2>

<table class="table table-striped">
  <thead>
    <tr>
      <th>Name</th>
      <th>Boeken</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($authors as $author):  ?>
      <tr>
        <td><a href="<?php echo \Util\Link::toRoute("author", [":id"=> $author->id]); ?> "><?php echo $author->name; ?></a></td>
        <td><?php echo count($author->books()); ?></a></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php if ($authorPaging): ?>
  <ul class="pagination pagination-sm">
    <?php if (!is_null($authorPaging->prev)): ?>
      <li class="previous">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->prev, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
          Vorige
        </a>
      </li>
    <?php endif; ?>

    <?php if (count($authorPaging->links()) > 1): ?>
      <?php foreach($authorPaging->links() as $link): ?>
        <li <?php echo $link == $authorPaging->current ? "class=active": ""; ?>>
          <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $link, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
            <?php echo $link; ?>
          </a>
        </li>
      <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!is_null($authorPaging->next)): ?>
      <li class="next">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->next, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
          Volgende
        </a>
      </li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
