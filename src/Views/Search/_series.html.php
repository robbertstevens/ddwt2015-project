<h2>Series</h2>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Naam</th>
      <th>Boeken</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($series as $serie):  ?>
      <tr>
        <td><a href="<?php echo \Util\Link::toRoute("user_profile", [":id"=> $user->id]); ?> "><?php echo $serie->name; ?></a></td>
        <td><?php echo count($serie->books()); ?></a></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php if ($seriePaging): ?>
  <ul class="pagination pagination-sm">
    <?php if (!is_null($seriePaging->prev)): ?>
      <li class="previous">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->prev]); ?>">
          Vorige
        </a>
      </li>
    <?php endif; ?>

    <?php if (count($seriePaging->links()) > 1): ?>
      <?php foreach($seriePaging->links() as $link): ?>
        <li <?php echo $link == $seriePaging->current ? "class=active": ""; ?>>
          <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $link]); ?>">
            <?php echo $link; ?>
          </a>
        </li>
      <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!is_null($seriePaging->next)): ?>
      <li class="next">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->next]); ?>">
          Volgende
        </a>
      </li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
