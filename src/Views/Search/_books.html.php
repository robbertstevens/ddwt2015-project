<h2>Boeken</h2>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Naam</th>
      <th>Omschrijving</th>
      <th>Pagina's</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($books as $book):  ?>
      <tr>
        <td><a href="<?php echo \Util\Link::toRoute("book", [":id"=> $book->id]); ?> "><?php echo $book->name; ?></a></td>
        <td><?php echo $book->description; ?></td>
        <td><?php echo $book->pages; ?> bladzijdes</td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php if ($bookPaging): ?>
  <ul class="pagination pagination-sm">
    <?php if (!is_null($bookPaging->prev)): ?>
      <li class="previous">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->prev, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
          Vorige
        </a>
      </li>
    <?php endif; ?>

    <?php if (count($bookPaging->links()) > 1): ?>
      <?php foreach($bookPaging->links() as $link): ?>
        <li <?php echo $link == $bookPaging->current ? "class=active": ""; ?>>
          <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $link, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
            <?php echo $link; ?>
          </a>
        </li>
      <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!is_null($bookPaging->next)): ?>
      <li class="next">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->next, "user" => $userPaging->current, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
          Volgende
        </a>
      </li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
