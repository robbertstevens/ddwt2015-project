<h2 id="users">Gebruikers</h2>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Naam</th>
      <th>Lees lijst</th>
      <th>Likes</th>
      <th>Vrienden</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($users as $user):  ?>
      <tr>
        <td><a href="<?php echo \Util\Link::toRoute("user_profile", [":id"=> $user->id]); ?> "><?php echo $user->name; ?></a></td>
        <td><?php echo count($user->readlist()); ?></a></td>
        <td><?php //echo count($user->likes()); ?></a></td>
        <td><?php //echo count($user->friends()); ?></a></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php if ($userPaging): ?>
  <ul class="pagination pagination-sm">
    <?php if (!is_null($userPaging->prev)): ?>
      <li class="previous">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->prev, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
          Vorige
        </a>
      </li>
    <?php endif; ?>

    <?php if (count($userPaging->links()) > 1): ?>
      <?php foreach($userPaging->links() as $link): ?>
        <li <?php echo $link == $userPaging->current ? "class=active": ""; ?>>
          <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $link, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
            <?php echo $link; ?>
          </a>
        </li>
      <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!is_null($userPaging->next)): ?>
      <li class="next">
        <a href="<?php echo url("search", [], ["search" => request()->get->search, "book" => $bookPaging->current, "user" => $userPaging->next, "author" =>  $authorPaging->current, "genre" =>  $genrePaging->current, "serie" =>  $seriePaging->current]); ?>">
          Volgende
        </a>
      </li>
    <?php endif; ?>
  </ul>
<?php endif; ?>
