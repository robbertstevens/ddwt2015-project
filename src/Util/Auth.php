<?php namespace Util;

/**
 * Makes a authentication class
 * @author Jorrit Bakker
 */

class Auth
{
    private static $instance;
    private function __construct()
    {

    }

    public static function getInstance()
    {
      if (self::$instance == null)
        return new static();
      else return self::$instance;
    }


    public function login($email, $password)
    {
      $users = new \Database\PDOUserRepository;
      $user = $users->findUserByEmail($email);
      if($user)
      {
        if($user->checkPassword($password))
        {
          $_SESSION["userId"] = $user->id;
          $_SESSION["expire"] = time()+(7*24*60*60);
          return true;
        }
      }
      return false;
    }

    public function logout()
    {
      session_destroy();
    }

    public function isLoggedIn()
    {
      if(isset($_SESSION["userId"]))
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    public function id(){
      if($this->isLoggedIn())
      {
        return $_SESSION["userId"];
      }
      else
      {
        return null;
      }
    }

    public function userIsAuthenticated($user)
    {
      if(isset($_SESSION["userId"]))
      {
          if($_SESSION["userId"] == $user->id){
            return true;
          }
      }
      return false;
    }

    /**
     * @return Models/User
     */
    public function user()
    {
        if($this->isLoggedIn())
        {
          $users = new \Database\PDOUserRepository;
          $user = $users->find($_SESSION["userId"]);
          return $user;
        }

        return null;
    }
}
