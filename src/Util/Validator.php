<?php namespace Util;

/**
 * Validator class for easy validations
 * @author Robbert Stevens
 */
class Validator
{
  private $modelFields = array();
  private $domain = array();

  /**
   * @var Array with errors
   */
  public $errors = array();

  public function __construct($model, $domain)
  {
    $this->modelFields = $this->findFields($model);
    $this->domain = $domain;
  }

  /**
   * Finds the fields that need to be validated in the model
   * @return Array
   */
  private function findFields($model)
  {
    $class = new \ReflectionClass($model);

    $array = array();
    foreach ($class->getProperties() as $field) {
      if ($field->isPublic())
        $array[$field->name] = $field->getValue($model);
    }

    return $array;
  }

  /**
   * Validates the model with the regex array
   * @return Bool true if validated
   */
  public function validate()
  {
    foreach($this->domain as $key => $field) {
      $regex  = "~" . $field . "~u";
      if (!preg_match($regex, $this->modelFields[$key])){
        $this->errors[] = $key;
      }
    }
    if (count($this->errors) > 0)
      return false;
    else
      return true;
  }

}
