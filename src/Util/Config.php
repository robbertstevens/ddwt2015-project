<?php namespace Util;

/**
 * Config class
 * @author Robbert Stevens
 */
class Config
{
    private static $instance;
    public $settings;
    private function __construct()
    {
        $this->settings = parse_ini_file("./config.ini");
    }


    /**
     * Easy way to get the dsn
     * @return string DSNs
     */
    public function dsn()
    {
        $dsn = 'mysql:host='.$this->settings["database"]["host"].';
                dbname='.$this->settings["database"]["dbname"].';
                port='.$this->settings["database"]["port"];

        return $dsn;
    }


    /**
     * create or get the instance of this object
     * @return Intance of this object
     */
    public static function getInstance()
    {
      if (self::$instance == null)
        return new static();
      else return self::$instance;
    }
}
