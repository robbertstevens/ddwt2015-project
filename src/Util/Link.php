<?php namespace Util;

/**
 * Utility class to create URLS
 * @author Robbert Stevens
 */
class Link {
  private function __construct() {}


  /**
   * Create an URL to the route
   * @param name of the route
   * @param Arguments if any
   * @return URL of the route
   */
  public static function toRoute($name, $args = [], $query = [])
  {
    $route = \Routes::lookup($name);
    //var_dump($query);
    if(count($query) > 0)
      $qs = "?". http_build_query($query);
    else $qs = "";
    return strtr($route->pattern, $args) . $qs ;
  }
}
