<?php namespace Util;

/**
 * Pagination class for easy pagination
 * @author Robbert Stevens
 */
class Paginate
{
    public $current;
    public $next;
    public $prev;
    public $pages;

    public $to;

    /**
     * @param $array full array of what has to be paginated
     * @param $currentPage the current page
     * @param $perPage how many items per page
     */
    public function __construct($array, $currentPage, $perPage)
    {
        $this->current = $currentPage;
        $this->pages = ceil(count($array) / $perPage);

        $this->prev = ($currentPage == 1 ? null : $currentPage - 1);
        $this->next = ($currentPage >= $this->pages ? null : $currentPage + 1);
    }
    /**
     * Returns array with the page links
     * @return Array ints
     */
    public function links()
    {
        $paginate = [];

        $links = 5;

        $start =( ( $this->current - $links ) > 0 ) ? $this->current - $links : 1;
        $end = ( ( $this->current + $links ) < $this->pages ) ? $this->current + $links : $this->pages;

        for ($i=$start; $i <= $end; $i++) {
            $paginate[] = $i;
        }

        return $paginate;
    }
}
