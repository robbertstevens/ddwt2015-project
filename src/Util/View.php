<?php namespace Util;

/**
 * Loads the template and extracts parameters
 * @author Jorrit Bakker
 * @version 0.1
 */

class View
{
	private $viewName;
	private $args;
	private $template;


	public function __construct($viewName, $args)
	{
		$this->viewName = $viewName;
		$this->args = $args;
		$this->args["request"] = request();
	}



	static function make($viewName, $args, $headers = array())
	{
		$view = new static($viewName, $args);


		return \Routing\Response::make($view, $headers);
	}
	public static function json($args)
	{
		$json[] = "Cache-Control: no-cache, must-revalidate";
    	$json[] = "Content-type: application/json";
    	return self::make("Global/json", $args, $json);
	}
	public function loadTemplate()
	{
		extract($this->args);

		include('src/Views/Global/header.html.php');
		include('src/Views/'.$this->viewName.'.html.php');
		include('src/Views/Global/footer.html.php');
	}

}
