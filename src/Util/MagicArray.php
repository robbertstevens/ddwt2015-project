<?php namespace Util;
/**
 * Transforms an array to an object
 * @author Robbert Stevens
 */
class MagicArray
{
  private $array;

  public function __construct($array)
  {
    $this->array = $array;
  }

  /**
   * Magic property to access the key of an array
   * @return Value of the array key, or null
   */
  public function __get($name)
  {
    if(array_key_exists($name, $this->array))
      return $this->array[$name];

    return null;
  }

  /**
   * Magic property to set an array key
   */
  public function _set($name, $value)
  {
    $this->array[$name] = $value;
  }
}
