<?php

/**
 * God class for routes ¯\_(ツ)_/¯
 * @author Robbert Stevens
 */
class Routes {
  private static $routes = array();

  /**
   * Cant instantiate this class
   */
  private function __construct() {}

  public static function set($name, $parameters)
  {
    self::addRoute($name, $parameters);
  }

  private static function addRoute($name, $parameters)
  {
    self::$routes[$name] = $parameters;
  }
  public static function lookUp($name)
  {
    return self::$routes[$name];
  }

  public static function getRoutes()
  {
    return self::$routes;
  }
}
