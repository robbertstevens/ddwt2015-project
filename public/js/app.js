$(document).ready(function(){
  var options = {
    url: "search/suggestions",
    getValue: "name",
		list: {
      maxNumberOfElements: 20,
      match: {
			     enabled: true
		  }
    },
    categories: [
        { listLocation: "books", header: "-- Boeken --"},
        { listLocation: "authors", header: "-- Auteurs --" },
        { listLocation: "series", header: "-- Series -- " },
        { listLocation: "users", header: "-- Gebruikers -- " },
        { listLocation: "genres", header: "-- Genres --" }
    ]
  }
  $("#film-search").easyAutocomplete(options);

  $("#like-button").text(function() {
    var button = $(this);
    var options = {
      url : "/like/status",
      dataType: "json",
      type: "POST",
      data: {
        "book_id" : $(this).data("book-id")
      },
      complete: function(data, text) {
        var data = JSON.parse(data.responseText)
        console.log(data);
        switch(data.message) {
          case "like": button.text("Like")
          break;
          case "unlike": button.text("Unlike")
          break
        }
      }
    }
    $.ajax(options);
  });
  $("#like-button").on("click", function(event) {
    event.preventDefault();
    var button = $(this);
    var likes = $("#likes");

    var options = {
      url : button.attr("href"),
      dataType: "json",
      type: "POST",
      data: {
        "book_id" : $(this).data("book-id")
      },
      succes: function(data,text) {
        console.log(text)
      },
      error: function(data) {
        console.log(data)
      },
      complete: function(data, text) {
        var data = JSON.parse(data.responseText)
        console.log(data);
        switch(data.message) {
          case "like":
            button.text("Unlike")
            likes.find("p").remove()
            likes.append("<a data-id='"+data.user_id+"' href='"+ data.user_url +"'><img class='img-circle' src='"+ data.avatar + "'</a>");
          break;

          case "unlike":
            button.text("Like");
            likes.find("[data-id='" + data.user_id + "']").remove();

            if (likes.find("[data-id]").length < 1)
              likes.append("<p> Niemand heeft dit boek nog geliked </p>");
          break;
        }
        button.blur();
      }
    }
    $.ajax(options);
  });

  $("#read-button").text(function() {
    var button = $(this);
    var options = {
      url : "/read/status",
      dataType: "json",
      type: "POST",
      data: {
        "book_id" : $(this).data("book-id")
      },
      complete: function(data, text) {
        var data = JSON.parse(data.responseText)
        console.log(data);
        switch(data.message) {
          case "read": button.text("Markeer als gelezen")
          break;
          case "unread": button.text("Markeer als niet gelezen")
          break
        }
      }
    }
    $.ajax(options);
  });
  $("#read-button").on("click", function(event) {
    event.preventDefault();
    var button = $(this);

    var options = {
      url : button.attr("href"),
      dataType: "json",
      type: "POST",
      data: {
        "book_id" : $(this).data("book-id")
      },
      succes: function(data,text) {
        console.log(text)
      },
      error: function(data) {
        console.log(data)
      },
      complete: function(data, text) {
        var data = JSON.parse(data.responseText)
        console.log(data);
        switch(data.message) {
          case "read":
            button.text("Markeer als niet gelezen")
          break;
          case "unread":
            button.text("Markeer als gelezen");
          break;
        }
        button.blur();
      }
    }
    $.ajax(options);
  });


  $("#readlist-button").text(function() {
    var button = $(this);
    var options = {
      url : "/readlist/status",
      dataType: "json",
      type: "POST",
      data: {
        "book_id" : $(this).data("book-id")
      },
      complete: function(data, text) {
        var data = JSON.parse(data.responseText)
        console.log(data);
        switch(data.message) {
          case "onReadlist": button.text("Toevoegen aan leeslijst")
          break;
          case "notOnReadlist": button.text("Verwijderen van leeslijst")
          break
        }
      }
    }
    $.ajax(options);
  });
  $("#readlist-button").on("click", function(event) {
    event.preventDefault();
    var button = $(this);

    var options = {
      url : button.attr("href"),
      dataType: "json",
      type: "POST",
      data: {
        "book_id" : $(this).data("book-id")
      },
      succes: function(data,text) {
        console.log(text)
      },
      error: function(data) {
        console.log(data)
      },
      complete: function(data, text) {
        var data = JSON.parse(data.responseText)
        console.log(data);
        switch(data.message) {
          case "onReadlist":
            button.text("Verwijderen van leeslijst")
          break;
          case "notOnReadlist":
            button.text("Toevoegen aan leeslijst");
          break;
        }
        button.blur();
      }
    }
    $.ajax(options);
  });
});
